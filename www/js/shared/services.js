/* Global appServices,appServices */
appServices.factory('Animate', function($ionicLoading, $ionicSlideBoxDelegate){
    return {
        loadStrt: function(duration) {
            $ionicLoading.show({
                content: 'Loading',
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showDelay: 0,
                duration: duration
            });
        },
        loadEnd: function() {
            $ionicLoading.hide();
        },
        slide: function() {
            return $ionicSlideBoxDelegate;
        } 
    }
});

appServices.factory ('StorageService', function(){
    return {
        get: function(stringValue){
            if (!localStorage[stringValue]) {
                return null;
            }else {
                var a = localStorage.getItem(stringValue);
                var b = JSON.parse(a);
                return b;
            }
        }, 
        set: function(stringValue, value, type, strObj) {
            var output = this.get(stringValue); 
          
            if (type == 'array') {
                output.push(value);
            } else if (type == 'obj') {
                output[strObj] = value;
            } else {
                output = value;
            }
            localStorage.setItem(stringValue, JSON.stringify(output));
        }
    }    
});

appServices.factory('LogInService', function(Auth, DataService){
    function _authUser() {
        return new Promise (function(resolve) {
            var a = Auth.$waitForSignIn();
            resolve(a);
        }).catch(function(e){
            console.log(e);
        });
    }  

    return {
        authData: function() {
            return new Promise (function(resolve) {
                var data = {};
                _authUser().then(function(res){
                    data.auth = res;
                    var uid = res.uid;
                    console.log('ok',uid);
                    DataService.getByUid('users', uid).then(function(r){
                        data.data = r;
                        resolve(data);
                    }), function(error){
                        console.log(error);
                    };
                }), function(error){
                    console.log(error);
                };
            }).catch(function(e){
                console.log(e);
            });
        },
        showBtn: function (caller, data) {
            data.showBtn = !data.showBtn;
            if (caller == '1') {
                data.showPassword = false;
            }else {
                data.showPassword = true;
            }
            return data;
        }
    }  
});

appServices.factory("Auth", ["$firebaseAuth",
                             function($firebaseAuth) {
                                 return $firebaseAuth();
                             }
]);

appServices.factory("Firebase", function() {
    var config = {
        apiKey: "AIzaSyC9y2lUx-nwErk2Newst8gnc_bgt03FJpY",
        authDomain: "mate03-e6acc.firebaseapp.com",
        databaseURL: "https://mate03-e6acc.firebaseio.com",
        storageBucket: "mate03-e6acc.appspot.com",
    };
    return firebase.initializeApp(config);
});

appServices.factory("Init", function(Utilities, PopupService) {
    function initUser(u, o, data, uid) {
        console.log(u, o, data, uid);
        var id =  u.name+u.firstName+u.phone;
        var imageName = Utilities.createRandomKey(5)+id;
        var user = {
            createdAt: data.createdAt,
            image: imageName,
            gender: u.gender,
            firstName: u.firstName,
            name: u.name,
            mail: u.mail,
            phone: u.phone,
            isSyndicMember: u.isSyndicMember,
            signaledAt: "",
            provider: "",
            formatedName: u.firstName+" "+u.name,
            role: "",
            signaledReason: "",
            id: uid,
            batiment: data.bId,
            immeuble: data.cp,
            flats: {
                cadastralParcel: data.cp,
                status: u.status?u.status:'owner',
                floor: u.floor
            }
        }
        if( u.birthday) {
            user.birthday = u.birthday;
        };
        if(!o.batName){
            user.batName = "";
        }else{
            user.batName = o.batName;
        }
        user.batimentIndex = {};
        user.immeubleIndex = {};
        user.batimentIndex[data.bId] = true;
        user.immeubleIndex[data.cp] = true;
        return user;
    }

    function initBat(data, o) {   
        var bat = {
            copro: data.cp,
            fullAddress: o.addressSelected,
            id: data.bId,
        }
        return bat;
    }

    function initFlat(data, u, uid, o) {
        var flat = {
            cadastralParcel: data.cp,
            createdAt: data.createdAt,
            postalCode: o.postalCode,
            network: {},
            bats: {},
        };
        flat.network[uid] = true;
        flat.bats[data.bId] = true;
        return flat;
    }

    return {
        initUser: function (user, o, data, uid) {
            return initUser (user, o, data, uid);
        },
        initBat: function (data, o) {
            return initBat(data, o);
        },
        initFlat: function (data, u, uid, o) { 
            return initFlat (data, u, uid, o);
        }
    }  
})

appServices.factory("UserService", function(DataService,Auth) {
    currentRef = rootRef.child('users');
    // var incidentsByUserRef = incidentsRef.orderByChild("userIndex/"+Auth.$getAuth().uid).equalTo(true);
    var User = {
        //profile:
    }
    var formatItem = function(o,k){
        var author=false;
        var batiment=false;
        var immeuble=false;
        var key=k;
        // var authorRef = ref.child('users/'+o.declaredBy);
        //var immeubleRef = ref.child('immeubles/'+o.copro);
        var immeubleRef = rootRef.child('immeubles/'+o.immeuble);
        var batimentRef = rootRef.child('batiments/'+o.batiment);
        //var author = DataService.load(authorRef);//DataService.getChild('users')[o.declaredBy];
        var immeuble = DataService.load(immeubleRef);//DataService.getChild('immeubles')[o.copro];
        var batiment = DataService.load(batimentRef);//DataService.getChild('immeubles')[o.copro];
        //var batiment = DataService.getChild('batiments')[o.declaredBy];
        o.key=key;
        var links = {};
        // links.author=author;
        links.immeuble=immeuble;
        links.batiment=batiment;
        o.links = links;
        return o;
        //    return o;

    }
    var all = function(key,value){
        var usersItemsAllRef =  currentRef;//.orderByChild("status").equalTo('solved');
        if(key && value){
            usersItemsAllRef = usersItemsAllRef.orderByChild(key).equalTo(value);
        }        

        return get(usersItemsAllRef);
    }

    var allByIndex = function(index,key,value){
        var usersItemsByIndexRef = currentRef.orderByChild(index+"/"+key).equalTo(value);
        return get(usersItemsByIndexRef);
    }

    var getByKey = function(key){
        //var usersItemsByKeyRef  = ref.child('users/'+key);
        var usersItemsByKeyRef  = rootRef.child('users').orderByKey().equalTo(key);

        //var itemsByKeyRef = currentRef.child('incidents');
        return get(usersItemsByKeyRef);
    }

    var get = function(ref){
        var obj = DataService.load(ref);//DataService.getChild('incidents');
        var data = {};
        obj.$loaded().then(function(){
            angular.forEach(obj,function(o,k){
                var item = formatItem(o,k);
                //console.log('dd',item,o,k);
                obj[k] = angular.extend({},item);
                //data[k]=incident;
            });
        })
        //console.log(obj);
        return obj;

    }

    var add = function(form){
        var data = { foo: "bar" };
        var post = angular.extend({},form,data);
        var userIndex = {};
        userIndex[post.user]=true;
        post.userIndex=userIndex;
        var newItem = DataService.add(incidentsRef,post);
        //console.log(newItem);
        newItem.then(function(itemId){
            //console.log(itemId);
            DataService.indexItem('users','incidents',itemId);
            //DataService.indexItem('incidents','users',itemId);

        })
    }

    var save = function(key,form){
        var itemRef = rootRef.child('users/'+key);
        //var data = { foo: "bar" };
        var post = angular.extend({},form/*,data*/);
        /*
        var userIndex = {};
        userIndex[post.user]=true;
        post.userIndex=userIndex;
        */
        var immeubleIndex = {};
        immeubleIndex[post.immeuble]=true;
        post.immeubleIndex=immeubleIndex;
        var batimentIndex = {};
        batimentIndex[post.batiment]=true;
        post.batimentIndex=batimentIndex;

        console.log('saving',key,post);
        //var data = { foo: "bar" };
        return DataService.save(itemRef,post);

    }

    var getById = function(_uid) {
        return DataService.getByUid('users', _uid);
    }

    var currentUser = {};
    var loggedUser = {};

    return {
        setCurrent: function(current) {
            currentUser = current;
        },
        getCurrent: function() {
            return currentUser;
        },
        setLogged: function(logged) {
            loggedUser = logged;
        },
        getLogged: function() {
            return loggedUser;
        },
        getById:function(_uid){
            return getById(_uid);
        },
        all:function(){
            return all();
        },
        allByIndex:function(index,key,value){
            return allByIndex(index,key,value);
        },
        get:function(ref){
            return get(ref);
        },
        getByKey:function(key){
            return getByKey(key);
        },
        add:function(data){
            return add(data);
        },
        save:function(key,form){
            return save(key,form);
        }
    }
});

appServices.factory("CreateUser", function(DataService, PopupService, Utilities, Auth, UserService, Init, Animate) {
    return {
        isIdentical: function() {
            return new Promise (function(resolve,reject) {
                var identicalMail = false;
                var identicalPhone = false;
                var users = DataService.fbArray('users');
                for (i=0; i<users.length; i++){
                    (function(iteration){
                        if (users[iteration].mail == newUser.mail){
                            identicalUser = true;
                        }else if (users[iteration].phone == newUser.phone){
                            identicalPhone = true;
                        }
                    })(i)
                }
                resolve([identicalMail, identicalPhone]);
            }).catch(function(e){
                PopupService.show('showError');
                Animate.loadEnd();
                //console.log(e);
            });
        },

        isNewCopro: function(cadastralParcel) {
            return new Promise (function(resolve, reject) {
                var existingBat = false;
                var existingFlat = false;
                var flats = DataService.fbArray('immeubles');
                for (i=0; i<flats.length; i++){
                    if (flats[i].cadastralParcel == cadastralParcel){
                        existingFlat = true;
                        var batsOfFlat = flats[i].bats;
                        for (bat in batsOfFlat){
                            if (batsOfFlat[bat].copro == cadastralParcel) {
                                existingBat = true;
                                //console.log('ce batiment existe déjà!')
                            }
                        }
                    } 
                }
                resolve([existingFlat, existingBat]); 
            }).catch(function(e){
                PopupService.show('showError');
                Animate.loadEnd();
                //console.log(e);
            })  
        },

        init: function(user, uid, o){
            return new Promise(function(resolve) {
                var data = {};
                data.createdAt = new Date().getTime();
                data.bId = Utilities.createRandomKey(23);
                data.cp = o.cadastralParcel;
                data.flatRef = 'immeubles/'+data.cp;
                var a = Init.initBat(data, o);
                data.bat = a;
                var b = Init.initFlat(data, user, uid, o);
                data.flat = b;
                var c = Init.initUser(user, o, data, uid);
                data.user = c; 

                resolve(data);
            }).catch(function(e){
                //console.log(e);
                Animate.loadEnd();
                PopupService.show('showError');
            });
        },

        editFlatUser: function(o, flatAndBat, uid) {
            return new Promise(function(resolve, reject){
                if(!flatAndBat[0]){ //si cet immeuble n'existe pas  
                    DataService.addCustom('immeubles/'+o.cp, o.flat);
                    DataService.addCustom('batiments/'+o.bId, o.bat);
                    // Si la copro existe déjà:
                }else if(!flatAndBat[1]){
                    // Si la copro existe déjà mais que le user s'inscrit dans un nouveau batiment
                    DataService.addCustom('batiments/'+o.bId, o.bat);
                    DataService.addCustom(o.flatRef+'/bats/'+o.bId, true);
                }  
                DataService.addCustom(o.flatRef+'/network/'+uid, true);
                // A son inscription, le user envoie une suggestion à tout les membres du réseau de la copro   
                DataService.addCustom('users/'+uid, o.user);
                resolve(o);
            }).catch(function(e){
                console.log(e);
                Animate.loadEnd();
                PopupService.show('showError');
            });
        },


        stepsDuringUserCreation: function (image, user, uid, userCopro) {
            var that = this;
            var flatAndBat = [];
            var resUser = {};
            return new Promise(function(resolve, reject) {  
                that.isNewCopro(userCopro.cadastralParcel).then(function(existing){
                    flatAndBat = existing;
                    that.init(user, uid, userCopro).then(function(data) {
                        that.editFlatUser(data, flatAndBat, uid).then(function(r){
                            resUser = r.user 
                            if (image !== 'css/img/icon-camera.png') {
                                Utilities.setImageReadyToExportToFirebase(image).then(function(result) {
                                    console.log('################# Service 445', result);
                                    Utilities.putImageInFirebaseStorage('images', resUser.image, result).then(function(res) {
                                        console.log('@@@@@@@@@@@@@@@@@@ Service 447', res);
                                        resolve(res);
                                    }), function(error){
                                        //console.log(error);
                                    };
                                }), function(error){
                                    //console.log(error);
                                };
                            }else {
                                console.log('################# Service 456', r);
                                resolve(r);
                            }
                        }), function(error){
                            //console.log(error);
                        };
                    }), function(error){
                        //console.log(error);
                    };
                }), function(error){
                    //console.log(error);
                };
            }).catch(function(e){
                //console.log(e);
            });                             
        },        

        firebaseCreateUser: function (user, userIsIdentical, image, userCopro) {
            //console.log(user, userIsIdentical, image, userCopro);
            var that = this;
            return new Promise(function(resolve,reject) {
                if(userIsIdentical[0] == false && userIsIdentical[1] == false){
                    Auth.$createUserWithEmailAndPassword(user.mail, user.pw).then(function(e){
                        var uid = e.uid;
                        user.id = uid;  
                        that.stepsDuringUserCreation(image, user, uid, userCopro).then(function(result){
                            PopupService.show('showSuccessInscription');
                            resolve(result);
                        }), function (error){
                            //console.log(error);
                        };
                    }).catch(function(firebaseError){
                        PopupService.show('alertCreateUserWithEmailAndPassword', firebaseError);
                        Animate.loadEnd();
                        //console.log(firebaseError);
                    })
                }else{
                    if (userIsIdentical[0] == true || userIsIdentical[1] == true) {
                        PopupService.show('alertIdenticalUser');
                    }
                    resolve(user);
                }
            }).catch(function(error){
                //console.log(error);
                Animate.loadEnd();
                PopupService.show('showError');
            });
        },

        createUserInFirebase: function(user, image, userCopro) {
            that = this;
            return new Promise(function(resolve, reject){
                that.isIdentical().then(function(identical){
                    that.firebaseCreateUser(user, identical, image, userCopro).then(function(userCreated){
                        resolve(userCreated);
                    }), function(error){
                        //console.log(error);
                        PopupService.show('alertIdenticalUser');
                    };
                }), function(error){
                    //console.log(error);
                    PopupService.show('alertIdenticalUser');
                };
            }).catch(function(e){
                //console.log(e);
                Animate.loadEnd();
                PopupService.show('alertIdenticalUser');
            });
        }  
    }   
});

appServices.factory("SignInService", function(IgnApiService) { 
    return {
      // initialisation des selects 'étage' et 'département'
      phoneIsCorrect: function (phone) {
          //var correct = true;
          for (i in phone){
              // pour s'assurer que que le numero donné est correctement formaté
              if(isNaN(parseInt(phone[i]))){
                  return false
              }
          }
          if (!phone || phone.length != 10 || phone[0] != 0 || (phone[1] != 6 && phone[1] != 7)){
              return false;
          }else {
              return true;
          }    
      },
      // initialisation des selects 'étage' et 'département'
      initSelects: function() {   
        var data = {
          genders: [
          {name: "Monsieur", value: "male"},
          {name: "Madame", value: "female"}
        ],
        status: [
          {name:"Propriétaire", value:"owner"},
          {name:"Locataire", value:"lodger"},
          {name:"Occupant", value:"occupier"},
          {name:"Propriétaire non occupant", value:"ownerNoOccupier"}
        ], 
          departements:[],
          floors: []
        }
        var a = this.initFloors();
        data.floors = a.floors;
        data.departements = a.departements;
        return data;
      },
      initFloors: function() {
        var data = { floors: [], departements: []};
        
        for(var i=-2; i<=40; i++){
          var j = {};
          if (i < 0){
            j.value = '-0'+Math.abs(i);
          }else if (i == 0){
            j.value = 'Rdc';
          }else if (i == 1){
            j.value = 'ES';
          } else if (i<10){
            j.value = '0'+i-1;
          }else {
            j.value = i-1;
          }
          data.floors.push(j);
        };
        
        for(var k=1; k<=97; k++){
          var l = {};
          if(k<10){
            l.value = '0'+k;
          }else{
            l.value = String(k);
          }
          if(k !== 96){
            data.departements.push(l);
          }  
        };
        return data;
      },   
      passwordLength: function (pw) {
          return pw.length >= 6;
      },
      getSuggestions: function(form, data){
          return IgnApiService.suggest(form, data);
      },
      setUserFullAdress: function(data, displayResult, form, result, cadastralParcel, city, addressSelected, showButton) {
          return IgnApiService.setUserFullAdress(data, displayResult, form, result, cadastralParcel, city, addressSelected, showButton);
      }
    }
});

appServices.factory("ImmeubleService", function(DataService,Auth) {
    immeublesRef = rootRef.child('immeubles');
    // var incidentsByUserRef = incidentsRef.orderByChild("userIndex/"+Auth.$getAuth().uid).equalTo(true);

    var formatItem = function(o,k){
        /*
        var author=false;
        var batiment=false;
        var immeuble=false;
        var key=k;
        var authorRef = ref.child('users/'+o.declaredBy);
        var immeubleRef = ref.child('immeubles/'+o.copro);
        var author = DataService.load(authorRef);//DataService.getChild('users')[o.declaredBy];
        var immeuble = DataService.load(immeubleRef);//DataService.getChild('immeubles')[o.copro];
        //var batiment = DataService.getChild('batiments')[o.declaredBy];
        o.key=key;
        o.author=author;
        o.immeuble=immeuble;
        */
        return o;

    }
    var all = function(){
        //currentRef = currentRef;//.orderByChild("status").equalTo('solved');

        return get(immeublesRef);
    }

    var allByIndex = function(index,key,value){
        var itemsByUserRef = currentRef.orderByChild(index+"/"+key).equalTo(value);
        return get(itemsByUserRef);
    }

    var get = function(ref){
        var obj = DataService.load(ref);//DataService.getChild('incidents');
        var data = {};
        obj.$loaded().then(function(){
            angular.forEach(obj,function(o,k){
                var item = formatItem(o,k);
                //console.log('dd',item,o,k);
                obj[k] = angular.extend({},item);
                //data[k]=incident;
            });
        })

        //console.log(obj);
        return obj;

    }

    var add = function(form){
        var data = { foo: "bar" };
        var post = angular.extend({},form,data);
        var userIndex = {};
        userIndex[post.user]=true;
        post.userIndex=userIndex;
        var newItem = DataService.add(incidentsRef,post);
        //console.log(newItem);
        newItem.then(function(itemId){
            //console.log(itemId);
            DataService.indexItem('users','incidents',itemId);
            //DataService.indexItem('incidents','users',itemId);

        })
    }
    var save = function(data){
        var data = { foo: "bar" };
        //DataService.save(ref,data);
    }
    return {
        all:function(){
            return all();
        },
        allByIndex:function(index,key,value){
            return allByIndex(index,key,value);
        },
        get:function(ref){
            return get(ref);
        },
        add:function(data){
            return add(data);
        },
        save:function(data){
            return save(data);
        }
    }
});

appServices.factory("BatimentService", function(DataService,Auth) {
    batimentsRef = rootRef.child('batiments');
    // var incidentsByUserRef = incidentsRef.orderByChild("userIndex/"+Auth.$getAuth().uid).equalTo(true);

    var formatItem = function(o,k){
        /*
        var author=false;
        var batiment=false;
        var immeuble=false;
        var key=k;
        var authorRef = ref.child('users/'+o.declaredBy);
        var immeubleRef = ref.child('immeubles/'+o.copro);
        var author = DataService.load(authorRef);//DataService.getChild('users')[o.declaredBy];
        var immeuble = DataService.load(immeubleRef);//DataService.getChild('immeubles')[o.copro];
        //var batiment = DataService.getChild('batiments')[o.declaredBy];
        o.key=key;
        o.author=author;
        o.immeuble=immeuble;
        */
        return o;

    }
    var all = function(){
        // currentRef = currentRef;//.orderByChild("status").equalTo('solved');

        return get(batimentsRef);
    }

    var allByIndex = function(index,key,value){
        var itemsByUserRef = currentRef.orderByChild(index+"/"+key).equalTo(value);
        return get(itemsByUserRef);
    }

    var get = function(ref){
        var obj = DataService.load(ref);//DataService.getChild('incidents');
        var data = {};
        obj.$loaded().then(function(){
            angular.forEach(obj,function(o,k){
                var item = formatItem(o,k);
                //console.log('dd',item,o,k);
                obj[k] = angular.extend({},item);
                //data[k]=incident;
            });
        })

        //console.log(obj);
        return obj;

    }

    var add = function(form){
        var data = { foo: "bar" };
        var post = angular.extend({},form,data);
        var userIndex = {};
        userIndex[post.user]=true;
        post.userIndex=userIndex;
        var newItem = DataService.add(incidentsRef,post);
        //console.log(newItem);
        newItem.then(function(itemId){
            //console.log(itemId);
            DataService.indexItem('users','incidents',itemId);
            //DataService.indexItem('incidents','users',itemId);

        })
    }
    var save = function(data){
        var data = { foo: "bar" };
        //DataService.save(ref,data);
    }
    return {
        all:function(){
            return all();
        },
        allByIndex:function(index,key,value){
            return allByIndex(index,key,value);
        },
        get:function(ref){
            return get(ref);
        },
        add:function(data){
            return add(data);
        },
        save:function(data){
            return save(data);
        }
    }
});

appServices.factory("IncidentService", function(DataService,Auth,CommentaireService) {
    var incidentsRef = rootRef.child('incidents');
    var incidentsByUserRef = incidentsRef.orderByChild("userIndex/"+Auth.$getAuth().uid).equalTo(true);

    var formatIncident = function(o,k){

        var author=false;
        var batiment=false;
        var immeuble=false;
        var key=k;
        var authorRef = rootRef.child('users/'+o.user);
        //var immeubleRef = ref.child('immeubles/'+o.copro);
        var immeubleRef = rootRef.child('immeubles/'+o.immeuble);
        var batimentRef = rootRef.child('batiments/'+o.batiment);
        var lastcommentaireRef = rootRef.child('commentaires').orderByChild("incidentIndex/"+key).equalTo(true).limitToFirst(1);
        var commentairesRef = rootRef.child('commentaires').orderByChild("incidentIndex/"+key).equalTo(true);
        var author = DataService.load(authorRef);//DataService.getChild('users')[o.declaredBy];
        var immeuble = DataService.load(immeubleRef);//DataService.getChild('immeubles')[o.copro];
        var batiment = DataService.load(batimentRef);//DataService.getChild('immeubles')[o.copro];
        var commentaires = DataService.loadAsArray(commentairesRef);//DataService.getChild('immeubles')[o.copro];
        var lastcommentaire = DataService.loadAsArray(lastcommentaireRef);//DataService.getChild('immeubles')[o.copro];
        //var batiment = DataService.getChild('batiments')[o.declaredBy];
        var index = 'incidentIndex';
        //var key = $stateParams.id;
        var value = true;
        o.incidentCommentairesList = CommentaireService.allByIndex(index,key,value);
        lastcommentaire.$loaded().then(function(){
            if(lastcommentaire.length){
               // console.log(lastcommentaire,lastcommentaire[0].$id);
               // o.last_comment = CommentaireService.getByKey(lastcommentaire[0].$id);
            }

        });
        o.key=key;
        o.last_comment = lastcommentaire;
        var links = {};
        links.author=author;
        links.immeuble=immeuble;
        links.batiment=batiment;
        links.commentaires=commentaires;
        o.links = links;
        return o;

    }
    var all = function(){
        incidentsRef = incidentsRef;//.orderByChild("status").equalTo('solved');

        return get(incidentsRef);
    }

    var allByIndex = function(index,key,value){
        var incidentsByUserRef = incidentsRef.orderByChild(index+"/"+key).equalTo(value);
        return get(incidentsByUserRef);
    }

    var getByKey = function(key){
        var incidentsByKeyRef  = rootRef.child('incidents').orderByKey().equalTo(key);
        // ref.child('incidents/'+key);
        /*
           var incidentsByKeyRef = firebase.database().ref('incidents/'+key);
        var incident = DataService.load(incidentsByKeyRef);//DataService.getChild('incidents');
        incident.$loaded().then(function(){
            console.log('dd',incident);

        });
        //var itemsByKeyRef = currentRef.child('incidents');
        return incident;
        */
        //var itemsByKeyRef = currentRef.child('incidents');
        return get(incidentsByKeyRef);
    }

    var get = function(ref){
        var incidents = DataService.load(ref);//DataService.getChild('incidents');
        var data = {};
        incidents.$loaded().then(function(){
            angular.forEach(incidents,function(o,k){
                var incident = formatIncident(o,k);
                //console.log('dd',incident,o,k);
                incidents[k] = angular.extend({},incident);
                //data[k]=incident;
            });
        })

        //console.log(incidents);
        return incidents;

    }

    var add = function(form){
        var data = { foo: "bar" };
        var post = angular.extend({},form,data);

        var userIndex = {};
        userIndex[post.user]=true;
        post.userIndex=userIndex;

        var immeubleIndex = {};
        immeubleIndex[post.immeuble]=true;
        post.immeubleIndex=immeubleIndex;

        var timestamp = new Date().getTime();
        post.timestamp=timestamp;

        var batimentIndex = {};
        batimentIndex[post.batiment]=true;
        post.batimentIndex=batimentIndex;

        var newItem = DataService.add(incidentsRef,post);
        //console.log(newItem);
        newItem.then(function(itemId){
            //console.log(itemId);
            DataService.indexItem('users','incidents',itemId);
            //DataService.indexItem('immeubles','incidents',itemId);
            //DataService.indexItem('incidents','users',itemId);

        })
    }
    var save = function(key,form){
        var itemRef = rootRef.child('incidents/'+key);

        var data = { foo: "bar" };
        var post = angular.extend({},form,data);

        var userIndex = {};
        userIndex[post.user]=true;
        post.userIndex=userIndex;

        var immeubleIndex = {};
        immeubleIndex[post.immeuble]=true;
        post.immeubleIndex=immeubleIndex;


        var batimentIndex = {};
        batimentIndex[post.batiment]=true;
        post.batimentIndex=batimentIndex;

        //console.log('saving',key,post);
        //var data = { foo: "bar" };
        DataService.save(itemRef,post);
    }
    return {
        all:function(){
            return all();
        },
        allByIndex:function(index,key,value){
            return allByIndex(index,key,value);
        },
        get:function(ref){
            return get(ref);
        },
        getByKey:function(key){
            return getByKey(key);
        },
        add:function(data){
            return add(data);
        },
        save:function(key,data){
            return save(key,data);
        }
    }
});

appServices.factory('DigipostService', function(Auth, DataService, Utilities, PopupService, StorageService, $q){
    // Recherche des digiposts qui ont en propriété 'copro', la parcelle cadastrale du currentUser
    var postsRef = DataService.getRef('postIts'); 
    var postsArray = [];

    return {

        set: function(scopePost){
            postsArray = scopePost;
        },
    
        get: function(){
            return postsArray;
        },

        add: function(posts, post, old){
            return new Promise(function(resolve, reject){
                if(old !== false) {
                    postsArray.splice(posts.indexOf(old), 1);
                }
                postsArray.push(angular.extend({}, post));
                resolve(postsArray);
            }).catch(function(e){
                console.log(e);
            });
        },

        edit: function(newPost, _user, old) {
            var deferred = $q.defer();
            var data = {};
            new Promise(function(resolve, reject){
                var post = {
                    content: newPost.content,
                    creator: newPost.creator,
                    creatorId: _user.id,
                    timeStamp: newPost.timeStamp,
                    date: Utilities.getFormatedDate(),
                    copro: _user.immeuble,
                    isSignaled: false
                }; 
                resolve(post);
            }).then(function(result){
                data.post = result;
                new Promise(function(resolve, reject){
                    if(old == false) {
                        var postKey = Utilities.createRandomKey(28);
                        result.id = postKey;    
                    }else{
                        var postKey = old.id;
                    } 
                    DataService.addCustom('postIts/'+postKey, result);
                    newTimeStamp = newPost.timeStamp;
                    StorageService.set('persistTimeStamp', newTimeStamp)
                    resolve(localStorage.persistTimeStamp); 
                }).then(function(result2){
                    PopupService.show('newPostAdded');
                    deferred.resolve(data.post);
                }).catch(function(err){
                    console.log(err);
                });
            }).catch(function(err){
                console.log(err);
                PopupService.show('showError');
            });
            return deferred.promise;
        }        
    }
});

appServices.factory("CommentaireService", function(DataService,Auth) {
    var commentairesRef = rootRef.child('commentaires');
    var commentairesByUserRef = commentairesRef.orderByChild("userIndex/"+Auth.$getAuth().uid).equalTo(true);
    // var commentairesByUserRef = incidentsRef.orderByChild("userIndex/"+Auth.$getAuth().uid).equalTo(true);

    var formatData = function(o,k){

        var author=false;
        var batiment=false;
        var immeuble=false;
        var incident=false;
        var key=k;
        var authorRef = rootRef.child('users/'+o.user);
        //var immeubleRef = ref.child('immeubles/'+o.copro);
        var immeubleRef = rootRef.child('immeubles/'+o.immeuble);
        var batimentRef = rootRef.child('batiments/'+o.batiment);
        var incidentRef = rootRef.child('incidents/'+o.incident);
        var author = DataService.load(authorRef);//DataService.getChild('users')[o.declaredBy];
        var immeuble = DataService.load(immeubleRef);//DataService.getChild('immeubles')[o.copro];
        var batiment = DataService.load(batimentRef);//DataService.getChild('immeubles')[o.copro];
        var incident = DataService.load(incidentRef);//DataService.getChild('immeubles')[o.copro];
        //var batiment = DataService.getChild('batiments')[o.declaredBy];
        o.key=key;
        var links = {};
        links.author=author;
        links.immeuble=immeuble;
        links.incident=incident;
        links.batiment=batiment;
        o.links = links;
        return o;

    }
    var all = function(){
        //incidentsRef = incidentsRef;//.orderByChild("status").equalTo('solved');

        return get(commentairesRef);
    }

    var allByIndex = function(index,key,value){
        var commentairesByIncidentRef = commentairesRef.orderByChild(index+"/"+key).equalTo(value);
        return get(commentairesByIncidentRef);
    }

    var getByKey = function(key){
        var commentairesByKeyRef  = rootRef.child('commentaires').orderByKey().equalTo(key);
        // ref.child('incidents/'+key);
        /*
           var incidentsByKeyRef = firebase.database().ref('incidents/'+key);
        var incident = DataService.load(incidentsByKeyRef);//DataService.getChild('incidents');
        incident.$loaded().then(function(){
            console.log('dd',incident);

        });
        //var itemsByKeyRef = currentRef.child('incidents');
        return incident;
        */
        //var itemsByKeyRef = currentRef.child('incidents');
        return get(commentairesByKeyRef);
    }

    var get = function(ref){
        var commentaires = DataService.load(ref);//DataService.getChild('incidents');
        var data = {};
        commentaires.$loaded().then(function(){
            angular.forEach(commentaires,function(o,k){
                var commentaire = formatData(o,k);
                //console.log('dd',incident,o,k);
                commentaires[k] = angular.extend({},commentaire);
                //data[k]=incident;
            });
        })

        //console.log(incidents);
        return commentaires;

    }

    var add = function(form){
        var data = { foo: "bar" };
        var post = angular.extend({},form,data);

        var userIndex = {};
        userIndex[post.user]=true;
        post.userIndex=userIndex;

        var immeubleIndex = {};
        immeubleIndex[post.immeuble]=true;
        post.immeubleIndex=immeubleIndex;

        var incidentIndex = {};
        incidentIndex[post.incident]=true;
        post.incidentIndex=incidentIndex;

        var timestamp = new Date().getTime();
        post.timestamp=timestamp;

        var batimentIndex = {};
        batimentIndex[post.batiment]=true;
        post.batimentIndex=batimentIndex;

        var newItem = DataService.add(commentairesRef,post);
        //console.log(newItem);
        newItem.then(function(itemId){
            //console.log(itemId);
            DataService.indexItem('incidents','commentaires',itemId);
            //DataService.indexItem('immeubles','incidents',itemId);
            //DataService.indexItem('incidents','users',itemId);

        })
    }
    var save = function(key,form){
        var itemRef = rootRef.child('commentaires/'+key);

        var data = { foo: "bar" };
        var post = angular.extend({},form,data);

        var userIndex = {};
        userIndex[post.user]=true;
        post.userIndex=userIndex;

        var immeubleIndex = {};
        immeubleIndex[post.immeuble]=true;
        post.immeubleIndex=immeubleIndex;


        var batimentIndex = {};
        batimentIndex[post.batiment]=true;
        post.batimentIndex=batimentIndex;

        //console.log('saving',key,post);
        //var data = { foo: "bar" };
        DataService.save(itemRef,post);
    }
    return {
        all:function(){
            return all();
        },
        allByIndex:function(index,key,value){
            return allByIndex(index,key,value);
        },
        get:function(ref){
            return get(ref);
        },
        getByKey:function(key){
            return getByKey(key);
        },
        add:function(data){
            return add(data);
        },
        save:function(key,data){
            return save(key,data);
        }
    }
});

appServices.factory("DataService", function($firebaseObject,$firebaseArray,Auth, $q) {
    var suggestions = function(uid){
      var deferred = $q.defer();
      var output = [];
      var ref_1 = rootRef.child('users/'+uid+'/beforeContacts');
      var beforeList = $firebaseArray(ref_1);
      var ref_2 = rootRef.child('users/')
      var users = $firebaseArray(ref_2);
      for(i=0; i < beforeList.length; i++) {
        var a = beforeList[i].$id;
        var rec = users.$getRecord(a);
        output.push(rec);
        if(i == beforeList.length-1) {
          deferred.resolve(output);
        }
      }
      return deferred.promise;
    }

    var Annonces = function(){
        return data.incidents;   
    }

    var initData = function(){
        var incidentsRef = rootRef.child('incidents');
        var incidentsByUserRef = incidentsRef.orderByChild("userIndex/"+Auth.$getAuth().uid).equalTo(true);
        return load(incidentsRef);
    }
    var data = {};
    var load = function(ref){
        var obj = $firebaseObject(ref);
        //var incidentsObj = $firebaseObject(incidentsRef);
        //var data = {};
        /*
        // to take an action after the data loads, use the $loaded() promise
        incidentsObj.$loaded().then(function(values) {
            console.log("loaded record:", values,incidentsObj);
        });
    */
        // to take an action after the data loads, use the $loaded() promise
        obj.$loaded().then(function(values) {
            // console.log("loaded record:", obj.$id, obj.someOtherKeyInData);

            // To iterate the key/value pairs of the object, use angular.forEach()
            angular.forEach(obj, function(value, key) {
                // console.log(key, value);
            });
        });
        // To make the data available in the DOM, assign it to $scope
        data = obj;

        return obj;//.$loaded();
        // For three-way data bindings, bind it to the scope instead
        // obj.$bindTo($scope, "data");
    }
    var loadAsArray = function(ref){
        var obj = $firebaseArray(ref);

        obj.$loaded().then(function(values) {
            // console.log("loaded record:", obj.$id, obj.someOtherKeyInData);

            // To iterate the key/value pairs of the object, use angular.forEach()
            angular.forEach(obj, function(value, key) {
                // console.log(key, value);
            });
        });
        data = obj;

        return obj;//.$loaded();

    }


    var add = function(ref,data){
        var list = $firebaseArray(ref);
        return list.$add(data).then(function(ref) {
            var id = ref.key;
            //console.log("added record with id " + id);
            list.$indexFor(id); // returns location in the array
            return id;
        });
    }
    var addCustom = function(str, data){
        var ref = rootRef.child(str);
        ref.set(data);
    }
    var indexItem = function(ref,type,key){
        //console.log('index',ref,type,key)
        var refUser = firebase.database().ref('users/'+Auth.$getAuth().uid+'/'+type);
        //+'/'+key
        var obj = $firebaseObject(refUser);
        obj.$loaded().then(function(values) {
            var d ={};
            obj[key]=true;
            //obj.usersIndex=d;
            //obj = angular.extend(obj,true);
            obj.$save().then(function(ref) {
                ref.key === obj.$id; // true
            }, function(error) {
                //console.log("Error:", error);
            });
        });

    }
    var save = function(ref,data){
        var deferred = $q.defer();
        var obj = $firebaseObject(ref);
        obj.$loaded().then(function(){
            angular.extend(obj,data);
            //obj.foo = "bar2";
            obj.$save().then(function() {
                ref.key === obj.$id; // true
                deferred.resolve(ref);
            }, function(error) {
                console.log("Error:", error);
            });
        });
        
        return deferred.promise;
    }
    var setKey = function(str){
        var ref = rootRef.child(str).push();
        return ref.key();

    }
    var getByProperty = function(ref, str, prop){
        var _ref = rootRef.child(ref);
        var output = [];
        return new Promise(function(resolve) {
            _ref.orderByChild(str).equalTo(prop).on('value', function(snapshot) {
                angular.forEach(snapshot.val(),function(o,k) {
                    output.push(o);
                });
                resolve(output);
            }), function(error) {
                console.log(error);
            };
        }).catch(function(e) {
            console.log(e);
        });
    }
    var getDigiPosts = function(user) {
        var deferred = $q.defer();
          var ref = rootRef.child('postIts');
          var postIts = $firebaseArray(ref);
          var output = [];
          postIts.$loaded(function (posts){
            for(i=0; i<posts.length; i++){
              if(posts[i].copro === user.immeuble){
                output.push(postIts[i]);
              }
            }
            deferred.resolve(output);
          }).catch(function () {
            deferred.reject();
          });
        return deferred.promise;
    }
    var getRef = function(str) {
        return rootRef.child(str);
    }
    var isEmpty = function(ref, child, val) {
        rootRef.child(ref).orderByChild(child).equalTo(val).on('value', function(snapshot) {
            var i = 0
            for (key in snapshot.val()){
                i ++;
            }
            return i === 0;
        });
    }
    var getOrderedBy = function(rf, child) {
        var deferred = $q.defer();
        var output = [];
        rootRef.child(rf).orderByChild(child).on('value', function(snapshot) {
            angular.forEach(snapshot.val(),function(o,k) {
               output.push(o); 
            });
            deferred.resolve(output);
        }), function(error) {
            console.log(error);
            deferred.reject(output);
        };
        return deferred.promise;
    }
    var deleteIt = function(item) {
        rootRef.child('postIts/'+item.$id).set(null);
    }

    return {
      delete: function(item) {
        return deleteIt(item);
      },
      getOrderedBy: function(rf, child) {
        return getOrderedBy(rf, child);
      },
      getDigiPosts: function (user) {
        return getDigiPosts(user);
      },
      get: function(user){
        return get(user);
      },
      getRef: function(str) {
        return getRef(str);
      },
      isEmpty: function(ref, child, val) {
        return isEmpty(ref, child, val);
      },
      suggestions: function(uid) {
        return suggestions(uid);
      },
      getByProperty: function(ref, str, prop) {
        return getByProperty(ref, str, prop);
      }, 
      addCustom: function(str, val) {
        return addCustom(str, val);
      },
      setKey: function(str) {
        return setKey(str);
      },
      fbArray: function(str) {
        return $firebaseArray(rootRef.child(str));
      },
      fbObj: function(str) {
        return $firebaseObj(rootRef.child(str));
      },
      initData:function(){
          return initData();
      },      
      load:function(ref){
          return load(ref);
      },      
      loadAsArray:function(ref){
          return loadAsArray(ref);
      },
      add:function(ref,data){
          return add(ref,data);
      },      
      indexItem:function(ref,type,key){
          return indexItem(ref,type,key);
      },      
      save:function(ref,data){
          return save(ref,data);
      },      
      data:function(){
          return data;
      },
      getChild:function(n){
          return data[n];

        },
        getParent:function(n){
            return data[n];
        }

        //Annonces();
    }
});

appServices.factory("LoggedInUser", function() {
    var Annonces = function(){
        return true;   
    };

    return Annonces;
});

appServices.factory ('IgnApiService', function ($q){
    return {

        formatString: function(location) {
            if(location[0]=='0'){  //si l'utilisateur tape '08' on trasforme ce résultat en '8' (l'API ne reconnait pas '08' mais '8')
                location = location.slice(1, location.length);
            }
            for(i=0; i<location.length; i++){ // si dans la string tapé on a un chiffre et que le caractère juste après existe, que c'est une lettre, que ce n'est pas un espace: 
                if (!isNaN(parseInt(location[i])) && isNaN(parseInt(location[i+1])) && location[i+1] && location[i+1] !== ' '){
                    var first = location.slice(0, i+1);
                    var last = location.slice(i+1, location.length);
                    var newLocation = first+' '+last; // on ajoute un espace entre le caractere chiffre et le caractere lettre
                    location = newLocation;
                }// le but est de pouvoir taper '2bis' alors que l'autocomplétion ne comprend que '2 bis'
            }
        },

        //fonction qui récupère dans l'API IGN toutes les adresses correspondant aux caractères tapés dans la barre de recherche
        suggest: function(form, data){
            var defer = $q.defer();
            var obj = {
                displayResult: false,
                showButton: false,
                str: "",
                suggestions: []
            };

            var location = form.typed; 
            var resultDiv = form.result; // balise html dans laquelle on affiche le 'select' des résultats
            //console.log(form.typed);
            this.formatString(location);

            var fo= {} ;
            fo.territory = [data.foString]; // data.foString est égal au numero de département sélectionné dans le 'select'
            fo.type = ["StreetAddress"]; // on filtre les résultats dans l'API qui ne cortrespondent qu'à des adresses de rue (pas de lieu dits genre 'tour eiffel...') 
            try {
                Gp.Services.autoComplete({
                    text: location,
                    apiKey: "22726iz9m8ficsgf2hmiicpd",
                    filterOptions: fo,
                    onSuccess: function(result) {
                        obj.displayResult = true; // boolean pour afficher le résultat dans ma balise html dont l'id est 'result'
                        obj.showButton = true; // le bouton pour valider le choix parmis les suggestions est affiché
                        var index= [];

                        if (result.suggestedLocations) {
                            for (i=0; i<result.suggestedLocations.length; i++) {
                                var loc= result.suggestedLocations[i] ;
                                obj.str+= '<option value="'+i+'">'+loc.fullText+'</option>';
                                obj.suggestions.push(loc);// le 'select' est un tableau des résultats sont implémentés avec le même index pour les résultats
                                obj.displayResult = true
                            }

                        }
                        if (location.length < 4){ // tant qu'il y a moins de 3 caractères tapés, on affiche pas le résultat des suggestions
                            obj.displayResult = false;
                            obj.showButton = false;

                        }else{    
                            obj.displayResult = true;
                            /*if(resultDiv){
                resultDiv.innerHTML = obj.str; 
              }*/
                        }
                        defer.resolve(obj);   
                    },
                    onFailure: function(error) {
                        //console.log(error);
                        defer.reject()
                    }
                });
            } catch (e) {}
            return defer.promise;
        },

        setUserFullAdress: function(data, form) { // Cette fonction complète la précédente: une fois qu'on a sélectionné une adresse suggérée, on 'redonne' cette adresse suggérée à l'API pour qu'elle nous retourne tous ses attributs (parcelle cadastrale, etc...)
            var defer = $q.defer();
            var obj = {
                displayResult: false,
                cadastralParcel: "",
                showButton: false,
                city: {},
                street: {},
                streetNumber: {},
                addressSelected: ""
            };

            obj.displayResult = false;

            var postalCode = data.foString;
            /*document.getElementById("city");*/ //*
            /*document.getElementById("street");*/ //**
            /*document.getElementById("streetNumber");*/ //*** : toutes les balises html correspondant aux champs à compléter par l'utilisatuer sont complétées avec les résultats de l'autocomplétion
            if (data.selectedSuggestion){
                obj.addressSelected = data.selectedSuggestion.fullText;
                ((function(addressSelected){   // fonction de l'API pour retourner la parcelle cadastrale et tous les autres attributs de l'adresse
                    Gp.Services.geocode({       // elle prend en paramètre la string de l'adresse sélectionnée
                        apiKey : "22726iz9m8ficsgf2hmiicpd",
                        location : addressSelected,  
                        filterOptions : {
                            type : ["StreetAddress"]
                        },
                        onSuccess : function (result) {  // en cas de succès de fonction: s'il y a match entre l'adresse donnée en paramètres et l'adresse qui existe dans l'API:

                            obj.postalCode = result.locations[0].placeAttributes.postalCode;  // dans result, ce qui nous intéresse, c'est l'attribut 'location', et plus particulièrement le 1er index
                            obj.city = result.locations[0].placeAttributes.municipality;     // c'est le match le plus précis par rapport à l'adresse rentrée en paramètres
                            obj.street = result.locations[0].placeAttributes.street;        // toutes le infos qui nous intéressent sont récupérées (rue, numéro, ville, code postal...)        
                            if(result.locations[0].placeAttributes.number != undefined){
                                obj.streetNumber = result.locations[0].placeAttributes.number;
                            }
                            var positionX = result.locations[0].position.x;  // les coordonnées GPS 'lat' et 'long' sont aussi récupérées pour être envoyées en paramètres de la fonction qui récupère la parcelle cadastrale 
                            var positionY = result.locations[0]. position.y;
                            ((function(positionX, positionY){

                                Gp.Services.reverseGeocode({
                                    apiKey : "22726iz9m8ficsgf2hmiicpd", 
                                    position : {                         
                                        x: positionY,
                                        y: positionX
                                    },
                                    filterOptions : {
                                        type : ["CadastralParcel"]    
                                    },
                                    onSuccess : function (result) {
                                        obj.cadastralParcel = result.locations[0].placeAttributes.cadastralParcel;
                                        obj.city = result.locations[0].placeAttributes.municipality;
                                        obj.show = true;

                                        defer.resolve(obj);
                                    }

                                });
                            }))(positionX, positionY);
                        }
                    });
                })(obj.addressSelected));
            }    
            obj.showButton = false;  
            return defer.promise;
        }

    }      
});

appServices.factory('Confirm', function (PopupService, $ionicPopup) {
    return {
        sendNewPassword: function(scope){
            var confirm = false;
            var myPopup = $ionicPopup.show({
                template: '<input type="email" ng-model="form.mail">',
                title: "Entrez votre adresse mail",
                subTitle: "Il s'agit de l'adresse qui vous sert d'identifiant dans cette application",
                scope: scope,
                buttons: [
                    { text: 'Annuler' },
                    {
                        text: '<b>Envoyer</b>',
                        type: 'button',
                        onTap: function(e) {
                            if(e){
                                confirm = true; 
                            }
                            if (!scope.form.mail) {
                                //console.log("pas d'email!")
                                e.preventDefault();
                            } else {  
                                return scope.form.mail;
                            }
                        }
                    }
                ]
            });
            myPopup.then(function(res) {
                if(confirm == true){
                    firebase.auth().sendPasswordResetEmail(scope.form.mail).then(function(){
                        PopupService.show('emailWasSentFromFirebase', scope.form.mail);
                    }).catch(function(){   
                        PopupService.show('displayFirebaseCodeError', scope.form.mail);
                        myPopup.close();   
                    });
                }    
            });
        }
    }    
});

appServices.factory('PopupService', function ($ionicPopup) {
  
  return{

    show: function (o, param, param2) {
      var a = this.popups(param, param2);
      var popup = a[o];
      $ionicPopup.alert({
        title: popup.title,
        template: popup.template
      });     
    },

    confirm: function () {

    },
    
    popups: function (param, param2){  
      var popup = {
          showPersistUser: {
              title: 'Session Active',
              template: 'Vous êtes actuellement connecté en tant que '+param
          }, 
          badlyFormatedEmail: {
              title: 'oh oh...',
              template: "Il semble qu'il y ait une erreur de saisie dans votre email."
          },
          alertPhoneProblem: {
              title: 'Numéro de téléphone incorrect :(',
              template: "Votre numéro ne semble pas correspondre à un numéro valide. Etes-vous sûr de l'avoir correctement saisi?"
          },
          alertIncomplete: {
              title: 'Infos incomplètes :(',
              template: "Il vous reste encore certains champs à compléter avant de pouvoir poursuivre."
          },
          mismatch: {
              title: 'Mots de passes ou mails non identiques',
              template: 'Vos deux adresses mails et vos deux mots de passe doivent correspondre'
          },
          showLimitedMokolesPerHour: {
              title: 'Délais trop court entre deux posts!',
              template: 'Vous pourrez coller un nouveau mot dans '+param+' minutes'
          },
          showLContactAlert: {
              title: 'Succès',
              template: 'Contact ajouté!'
          },
          alertCreateUserWithEmailAndPassword: {
              title: param,
              template: param
          },
          alertCameraFailure: {
              title: 'Erreur de la photo',
              template: 'Causes de cette erreur: ' + param
          },
          alertIdenticalUser: {
              title: 'Utilisateur déjà enregistré!',
              template:  "Les informations que vous avez fournies sont identiques à celles d'un autre utlisateur: nous ne pouvons donc vous enregistrer."
          },
          alreadySelectedForChat: {
              title: 'Utilisateur déjà choisi(e)',
              template: "Vous avez déjà sélectionné "+param+" pour faire partie de cette nouvelle discussion."
          },
          showError: {
              title: 'Une erreur est survenue',
              template: "L'opération n'a pu aboutir, veuillez recommencer."
          },
          showIncomplete: {
              title: 'Informations incomplètes',
              template: "L'étage et le lieu de l'incident sont nécessaires à sa déclaration."
          },
          showSuccessInscription: {
              title: 'Félicitations!',
              template: "Vous rejoignez désormais la communauté MY Spoty Home!"
          },
          displayFirebaseCodeError: {
              title: "Erreur lors de la saisie de votre adresse mail",
              template: "Assurez-vous d'avoir correctement saisi votre adresse mail dans 'identifiant'. Cette adresse doit être celle que vous utilisez pour vous connecter à cette application."
          },
          emailWasSentFromFirebase: {
              title: "Email envoyé avec succès!",
              template: "Un lien pour réinitialiser votre mot de passe vous a été envoyé à l'adresse "+param+" ."
          },
          showProvider: {
              title: "Provider:",
              template: param
          },
          showSendInvitation: {
              title:  'Inviter '+param,
              template: 'Souhaitez-vous inviter '+param2+' à rejoindre votre réseau?'
          },
          showContactAlreadyAdded: {
              title: "Contact déjà existant.",
              template: param+" fait dèjà parti de vos contacts."
          },
          selectAFile: {
              title: "Aucune photo choisie",
              template: "Vous devez choisir une photo pour modifier votre profile."
          },
          showIncidentUpdatedTooSoon: {
              title: 'Echec mise à jour incident',
              template: "Un même incident ne peut pas être mis à jour plus d'une fois par heure! Dernière mise à jour: il y a "+Math.round(param/60)+" minutes."
          },
          showClosedIncident: {
              title: "Félicitation",
              template: "Suite à votre demande, cet incident a bien été clos."
          },
          showMustAddDescription: {
              title: 'Echec de mise à jour incident',
              template: "Vous devez ajouter des précisions pour que votre modification soit prise en compte."
          },
          newPostAdded: {
              title: 'Félicitations!',
              template: "Vous venez de poster un nouveau message pour votre immeuble."
          },
          incidentUpdated: {
              title: 'Félicitations!',
              template: "Votre mise à jour de l'incident est désormais accessible à tous les membres de votre copropriété!"
          },
          incidentAdded: {
              title: 'Nouvel incident créé',
              template: "Le nouvel incident que vous avez créé est porté a l'attention de votre syndic. Il est aussi visible par tous les membres de votre immeuble."
          },
          wrongMail: {
              title: "Adresse mail inconnue",
              template: "Nous ne reconnaissons pas cette adresse. Assurez-vous d'avoir saisi la bonne adresse mail."
          },
          wrongPassword: {
              title: "Mot de passe incorrect",
              template: "Assurez-vous d'avoir saisi le bon mot de passe. Si vous navez oublié votre mot de passe vous pouvez cliquer sur 'mot de passe oublié'"
          },
          verificationEmailHAsBeenSent: {
              title: "Confirmation de votre adresse mail",
              template: "Un email de confirmation vous a été envoyé à l'adresse mail que vous avez indiquée lors de la création de votre compte. Suivez le lien d'activation pour valider votre compte et accéder à l'application."
          },
          redirectToLogin: {
              title: "Adresse mail déjà utilisée",
              template: "Cette adresse mail est déjà associée à un compte. Vous pouvez vous essayer de vous connecter avec pour accéder au service ou finir de compléter les information de votre inscription"  
          },
          pwIsTooShort: {
              title: "Mot de pase trop court",
              template: "Votre mot de pase doit être égal à au moins six caractères."  
          },
          test: {
              title: "erreur: ",
              template: param  
          },
          underAge: {
              title: "Moins de 18 ans",
              template: "Vous devez être majeur pour accéder aux services de cette application."  
          }
        }
        return popup;
      }
    }    
});