appControllers.directive('fileInput', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attributes) {
            element.bind('change', function () {
                $parse(attributes.fileInput)
                    .assign(scope,element[0].files)
                scope.$apply()
            });
        }
    };
}]);

appControllers.directive("showPassword", function() { 
    return function linkFn(scope, elem, attrs) {
        scope.$watch(attrs.showPassword, function(newValue) {
            scope.test = !scope.test;
            if (newValue) {
                elem.attr("type", "text");
            } else {
                elem.attr("type", "password");
            };
        });
    };
});

//This is Controller for Dialog box.
appControllers.controller('DialogController', function ($scope, $mdDialog, displayOption) {

    //This variable for display wording of dialog.
    //object schema:
    //displayOption: {
    //        title: "Confirm to remove all data?",
    //        content: "All data will remove from local storage.",
    //        ok: "Confirm",
    //        cancel: "Close"
    //}
    $scope.displayOption = displayOption;

    $scope.cancel = function () {
        $mdDialog.cancel(); //close dialog.
    };

    $scope.ok = function () {
        $mdDialog.hide();//hide dialog.
    };

});// End Controller for Dialog box.

//Controller for Toast.
appControllers.controller('toastController', function ($scope, displayOption) {

    //this variable for display wording of toast.
    //object schema:
    // displayOption: {
    //    title: "Data Saved !"
    //}

    $scope.displayOption = displayOption;

});// End Controller for Toast.

//Controller for Toast.
appControllers.controller('authController', function ($scope, $state, Auth, PopupService, Animate, Confirm, Utilities, localStorage, DataService, LogInService) {
    $scope.data = {
        showBtn: true,
        showPassword: false
    }  

    $scope.showPW = function(caller) {
        LogInService.showBtn(caller, $scope.data);
    }

    $scope.form = {
        mail: "",
        password: ""
    }
    $scope.loginUser = function(form) {
        if(Utilities.isIncorrectValue([form.mail, form.password])) {
            PopupService.show('alertIncomplete');
        } else {
            Auth.$signInWithEmailAndPassword(form.mail, form.password)
                .then(function(authData) {

            }).catch(function(error) {
                console.log(error.code);
                if (error.code == "auth/user-not-found"){
                    PopupService.show('wrongMail');
                }else if (error.code == "auth/invalid-email") {
                    PopupService.show('badlyFormatedEmail');
                }else if (error.code == "auth/wrong-password"){
                    PopupService.show('wrongPassword');
                }
            });
        }
    }  

    $scope.loginUserWithFb = function(user) {
        var provider = new firebase.auth.FacebookAuthProvider();

        firebase.auth().signInWithRedirect(provider).then(function(result) {
            // This gives you a Facebook Access Token. You can use it to access the Facebook API.
            var token = result.credential.accessToken;
            // The signed-in user info.
            var user = result.user;
            // ...

            console.log('SUCCESS',result);
        }).catch(function(error) {
            // Handle Errors here.
            console.log('FAIL',error);
            var errorCode = error.code;
            var errorMessage = error.message;
            // The email of the user's account used.
            var mail = error.email;
            // The firebase.auth.AuthCredential type that was used.
            var credential = error.credential;
            // ...
        });
    }

    $scope.loginUserWithGoogle = function(user) {
        var provider = new firebase.auth.GoogleAuthProvider();

        firebase.auth().signInWithRedirect(provider).then(function(result) {
            // This gives you a Facebook Access Token. You can use it to access the Facebook API.
            var token = result.credential.accessToken;
            // The signed-in user info.
            var user = result.user;
            // ...

            console.log('SUCCESS',result);
        }).catch(function(error) {
            // Handle Errors here.
            console.log('FAIL',error);
            var errorCode = error.code;
            var errorMessage = error.message;
            // The email of the user's account used.
            var email = error.email;
            // The firebase.auth.AuthCredential type that was used.
            var credential = error.credential;
            // ...
        });
    }

    $scope.slide = function(to) {
        $scope.current = to;
        Animate.slide().slide(to);
    }
    $scope.back = function(index){
        $scope.slide(index);
    }

});

appControllers.controller('wizzardSignUpController', function ($scope, $state, Auth, Utilities, PopupService, SignInService, CreateUser, Animate, $ionicActionSheet, $timeout, LogInService, UserService, $cordovaDatePicker) {  
    $scope.b_Day = [];
    $scope.getAge = function () {
        var options = {
            date: new Date(),
            mode: 'date', // ou 'time' pour une heure
            allowOldDates: true,
            allowFutureDates: false,
            doneButtonLabel: 'DONE',
            doneButtonColor: '#F2F3F4',
            cancelButtonLabel: 'CANCEL',
            cancelButtonColor: '#000000'
        };
        $cordovaDatePicker.show(options).then(function(date){
            var a  = new String(date);
            var from = a.split(" ");
            var day = $scope.date.day[from[0].toLowerCase()];
            var month = $scope.date.month[from[1].toLowerCase()];
            var date = from[2];
            var year = from[3];
            
            $scope.b_Day[0] = year; 
            $scope.b_Day[1] = String(month[0]); 
            $scope.b_Day[2] = date;
            $scope.form.user.birthday = day[1] +" "+date+" "+month[1]+" "+year;
            $scope.display = true;
        }).catch(function(e) {
            console.log(e);
        });
    }

    $scope.majority = function(){
        var f = new Date(parseInt($scope.b_Day[0]), parseInt($scope.b_Day[1]), parseInt($scope.b_Day[2]));
        $scope.isEighteen = Utilities.isEighteen(f);
        return $scope.isEighteen;
    }
    
    $scope.date = {
        day: {
            sun: [0, 'dimanche'], mon: [1, 'lundi'], tue: [2, 'mardi'],
            wed: [3, 'mercredi'], thu: [4, 'jeudi'], fri: [5, 'vendredi'], 
            sat: [6, 'samedi']
        },
        month: {
            jan: [0, 'janvier'], feb: [1, 'févirer'], mar: [2, 'mars'], 
            apr: [3, 'avril'], may: [4, 'mai'], jun: [5, 'juin'], 
            jul: [6, 'jullet'], aug: [7, 'aout'], sep: [8, 'septembre'], 
            oct: [9, 'octobre'], nov: [10, 'novembre'], dec: [11, 'décembre']
        }   
    };

    $scope.isEighteen = false;
    $scope.confirmEighteen;
    $scope.display = false;

    $scope.data = {
        showBtn: true,
        showPassword: false
    }  

    $scope.showPW = function(caller) {
        LogInService.showBtn(caller, $scope.data);
    }

    $scope.img = {
        src: 'css/img/icon-camera.png'
    }

    /*$scope.refresh = function() {
    $timeout(function() {
      window.location.reload(true);
    });
  }*/

    $scope.takePicture = function(index, img) {    
        Utilities.takePicture(index, img).then(function(result){

            $scope.img.src = result;
        }), function(error){
            console.log(error);
        };    
    };

    $scope.takePhotoActionSheet = function(img) {

        var hideSheet = $ionicActionSheet.show({
            buttons: [
                { text: 'Choisir photo existante' },
                { text: 'Prendre une nouvelle photo' }
            ],
            cancelText: 'Cancel',
            cancel: function() {

            },
            buttonClicked: function(index) {
                $scope.takePicture(index, img);
                return true;
            }
        });
    };    

    $scope.goBackSlide = function() {
        var index = Animate.slide().currentIndex();
        if (index>0) {
            Animate.slide().slide(index-1);
        } else {
            //Animate.slide().slide(3);
            $state.go('public.tryApp');
        }
    }    

    $scope.$on( "$ionicView.beforreLeave", function(scopes, states) { 
        $scope.showCodeSyndic = false;
    });

    $scope.data = SignInService.initSelects();

    $scope.showCodeSyndic = false;

    $scope.form = {
        syndicNumber: "",
        user: {
            isSyndicMember: false,
            birthday: false
        }
    };
    $scope.form.user = angular.extend({},$scope.currentUser);
    $scope.form.user.isSyndicMember = false;

    $scope.o = {};

    $scope.passValide = false;

    $scope.lockSlide = function () {
        Animate.slide().enableSlide( false );
    }

    /*$scope.refresh = function() {
    console.log($scope.image);
    var blobUrl = URL.createObjectURL($scope.image);
    window.location = blobUrl; 
  }*/

    $scope.readURL = function(scopeImage, str) {
        Utilities.readURL(scopeImage, str);
        $scope.form.user.image = scopeImage; 
    }

    $scope.showCodeSyndicFunc = function(){
        $scope.showCodeSyndic = !$scope.showCodeSyndic;
    } 

    $scope.implement_0 = function(is_18) {
        Animate.slide().slide(1);
    }

    $scope.implement_1 = function(o) {
        if(Utilities.isIncorrectValue([o.mail, o.pw])) {
            PopupService.show('alertIncomplete');
        } else if (/*o.mail !== o.email_2 || */o.pw !== o.pw_2) {
            PopupService.show('mismatch');
        } else if (!SignInService.phoneIsCorrect(o.phone)) {
            PopupService.show('alertPhoneProblem');
        } else if(!SignInService.passwordLength(o.pw)) {
            PopupService.show('pwIsTooShort');
        }else {
            Animate.slide().slide(2);
        }
    } 

    $scope.implement_2 = function (user) {
        for(k in user){
            if (Utilities.isIncorrectValue(user[k]) && k !== 'batName' && k !== 'batiment') {
                PopupService.show('alertIncomplete');
            } else {
                Animate.slide().slide(3);
            }
        }
    }  

    $scope.suggest = function (form, data) {
        SignInService.getSuggestions(form, data).then(function(obj) {
            $scope.displayResult = obj.displayResult;
            $scope.showButton = obj.showButton;
            if(obj.suggestions && obj.suggestions.length > 0){
                $scope.suggestions = obj.suggestions;
            }  
        });
    }

    $scope.setUserFullAdress = function() {
        SignInService.setUserFullAdress($scope.data, $scope.form).then(function(obj) {
            $scope.displayResult = obj.displayResult;
            $scope.o = obj;
            $scope.showButton = obj.showButton;
        }), function (e){
            //console.log(e);
        }
    }
    $scope.saveCompleteProfile = function (){
        //Animate.loadStrt();
        $scope.form.user.links = {};
        var uid = $scope.loggedInUser.uid;
        var userCopro = $scope.o;
        var user = $scope.form.user;
        var profile = UserService.save(uid,user);

        profile.then(function(dataRes){
            console.log(profile,dataRes);
            CreateUser.stepsDuringUserCreation('', user, uid, userCopro).then(function(res){
                //Animate.loadEnd();
                console.log('done',res);
                $state.go('app.homeDashboard');
            }), function (e) {
                //console.log(e);
            }   
            $state.go('app.homeDashboard');

        })
        return ;


    }  
    $scope.createUser = function (user, image, o){
        Animate.loadStrt(120000);
        CreateUser.createUserInFirebase(user, image, o).then(function(res){
            Animate.loadEnd();
            $state.go('app.homeDashboard');
        }), function (e) {
            //console.log(e);
        }   
    }  
});


appControllers.controller('userProfileCtrl', function ($scope, Auth,$state,localStorage,ImmeubleService,BatimentService,$mdBottomSheet,$mdDialog,UserService) {
    $scope.userInfo = localStorage.get("LoggedInUser");
    $scope.form = {};
    $scope.logout = function() {
        //console.log('logout');
        Auth.$signOut();
        $state.go('app.fakeLogin');
    };
    $scope.editProfile = function() {
        //console.log('editUserProfile');
        //Auth.$signOut();
        $state.go('app.editUserProfile');
    };  
    console.log($scope.currentUser,$scope.userInfo, Auth.$getAuth());
    // value="form.uid"
    if($scope.currentUser!==undefined){
        $scope.form.uid = $scope.currentUser.id;
        $scope.form.mail = $scope.currentUser.mail;
        $scope.form.formatedName = $scope.currentUser.formatedName;
        $scope.form.batiment=$scope.currentUser.batiment;
        $scope.form.immeuble=$scope.currentUser.immeuble;     
    }


    // contractForm(object) = contract object that presenting on the view.
    $scope.showEditProfileBottomSheet = function ($event, contractForm) {
        //console.log('showEditProfileBottomSheet');
        // $scope.disableSaveBtn = $scope.validateRequiredField(contractForm);
        $mdBottomSheet.show({
            templateUrl: 'incident-actions-template.html',
            targetEvent: $event,
            scope: $scope.$new(false),
        });
    };// End showing the bottom sheet.

    $scope.saveProfile = function (form, $event) {
        //$mdBottomSheet.hide() use for hide bottom sheet.
        $mdBottomSheet.hide();
        //mdDialog.show use for show alert box for Confirm to save data.
        $mdDialog.show({
            controller: 'DialogController',
            templateUrl: 'confirm-dialog.html',
            targetEvent: $event,
            locals: {
                displayOption: {
                    title: "Confirm to save incident?",
                    content: "Data will save to Fb.",
                    ok: "Confirmer",
                    cancel: "Fermer"
                }
            }
        }).then(function () {
            //$scope.form.user = $scope.loggedInUser.uid;
            $scope.form.links = {};
            //UserService.add($scope.form);
            UserService.save($scope.userInfo.uid,$scope.form);
            // IncidentService.add($scope.form);
            return ;
        });
    };
});// End Controller for Toast.
//Controller for Toast.
//Controller for Toast.

appControllers.controller('networkController', function ($scope, DataService) {

});

appControllers.controller('ZZZZauthController', function ($scope,Auth,$state,localStorage) {

    //TODO : Voisin
    //TODO : Contacts
    //TODO : Invitations / Acceptations
    //INPROGRESS : Réseaux 
    //INPROGRESS : Chat
    //INPROGRESS : Incidents + Edition + Commentaires
    //INPROGRESS : FB, G+, Mail Connect
    //INPROGRESS : Refresh Incidents / Annonces 
    //INPROGRESS : Notifications
    //


    //this variable for display wording of toast.
    //object schema:
    // displayOption: {
    //    title: "Data Saved !"
    //}

    $scope.error = null;
    $scope.register = false;
    $scope.loggedInUser = false;

    /**********************************
   * [FUNCTIONS] UTILS
   *********************************/

    $scope.isIncorrectValue = function(val) {
        return angular.isUndefined(val) || val === null || val == "";
    }

    $scope.cleanVariables = function() {
        $scope.error = null;
    }
    $scope.loginUser = function(user) {
        if($scope.isIncorrectValue(user.mail) || $scope.isIncorrectValue(user.password)) {
            $scope.error = "Vous avez oublié de remplir des champs ...";
        } else {
            Auth.$signInWithEmailAndPassword(user.mail, user.password)
                .then(function(authData) {

                $scope.loggedInUser = authData;
            }).catch(function(error) {
                $scope.error = ""+error;
            });
        }
    }



    $scope.loginUserWithFb = function(user) {
        var provider = new firebase.auth.FacebookAuthProvider();

        firebase.auth().signInWithRedirect(provider).then(function(result) {
            // This gives you a Facebook Access Token. You can use it to access the Facebook API.
            var token = result.credential.accessToken;
            // The signed-in user info.
            var user = result.user;
            // ...

            //console.log('SUCCESS',result);
        }).catch(function(error) {
            // Handle Errors here.
            //console.log('FAIL',error);
            var errorCode = error.code;
            var errorMessage = error.message;
            // The email of the user's account used.
            var mail = error.email;
            // The firebase.auth.AuthCredential type that was used.
            var credential = error.credential;
            // ...
        });
    }

    $scope.loginUserWithGoogle = function(user) {
        var provider = new firebase.auth.GoogleAuthProvider();

        firebase.auth().signInWithRedirect(provider).then(function(result) {
            // This gives you a Facebook Access Token. You can use it to access the Facebook API.
            var token = result.credential.accessToken;
            // The signed-in user info.
            var user = result.user;
            // ...

            //console.log('SUCCESS',result);
        }).catch(function(error) {
            // Handle Errors here.
            //console.log('FAIL',error);
            var errorCode = error.code;
            var errorMessage = error.message;
            // The email of the user's account used.
            var email = error.email;
            // The firebase.auth.AuthCredential type that was used.
            var credential = error.credential;
            // ...
        });
    }

    $scope.registerUser = function(user) {
        //console.log('registerUser',user);
        if($scope.isIncorrectValue(user.mail) || $scope.isIncorrectValue(user.password)) {
            $scope.error = "Vous avez oublié de remplir des champs ...";
        } else {
            Auth.$createUserWithEmailAndPassword(user.mail, user.password)
                .then(function(authData) {

                $scope.loggedInUser = authData;
            }).catch(function(error) {
                $scope.error = ""+error;
            });
        }
    }

    $scope.logout = function() {
        Auth.$signOut();
    };
    //console.log(Auth.$getAuth(),localStorage.get("LoggedInUser"));
    //Auth.$signOut();
    /*
    Auth.$onAuthStateChanged(function(authData) {
        if (authData) {
            // Define signed user
            console.log("Signed in as :", authData.email);
            $scope.loggedInUser = authData;
            // Close register modal in register case
            // $scope.closeRegister();
            // Redirect correct page
            console.log("Signed In");
            if($scope.register) {
                //$location.path('/slide');
            } else {
                / *
                $scope.userInfo = {
                            name: result.data.name,
                            first_name: result.data.first_name,
                            last_name: result.data.last_name,
                            email: result.data.email,
                            birthday: result.data.birthday,
                            link: result.data.link,
                            cover: result.data.cover,
                            pictureProfileUrl: "http://graph.facebook.com/" + result.data.id + "/picture?width=500&height=500",
                            gender: result.data.gender,
                            id: result.data.id,
                            access_token: $scope.accessToken
                        };
                        * /
                // Store user profile information to localStorage service.

                // $location.path('/tab/photo');

                $state.go('app.homeDashboard');

            }
        } else {
            console.log("Signed out");
            $scope.loggedInUser = null;
        }
        localStorage.set("LoggedInUser", $scope.loggedInUser);
    });
    console.log(Auth);
    */

});// End Controller for Toast.


