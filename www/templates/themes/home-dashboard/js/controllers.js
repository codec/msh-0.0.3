// Controller of menu dashboard page.
appControllers.controller('homeDashboardCtrl', function ($scope, $mdToast,$state,Auth,localStorage,DataService,IncidentService,ImmeubleService,UserService,$timeout) {

    $scope.$on( "$ionicView.enter", function() { 
        /*if ($scope.currentUser && $scope.currentUser.batiment === undefined){
            $state.go('public.fakeWizardSignUp');
        }else*/ 
        $timeout($timeout(
            function() {
               if (!$scope.currentUser || !$scope.currentUser.batiment){
                 //   $state.go('public.tryApp');
                }             
             },200)
        );

    })
   //console.log(UserService.getByKey($scope.loggedInUser.uid)); //console.log($scope.loggedInUser.uid,DataService.getChild('incidents'),DataService.getChild('users/'+$scope.loggedInUser.uid),IncidentService.save());
   /*$scope.$on( "$ionicView.enter", function(scopes, states) { 
        if (!$scope.currentUser){
            $state.go('public.fakeWizardSignUp');
        }
    })*/
   
    //ShowToast for show toast when user press button.
    $scope.showToast = function (menuName) {
        //Calling $mdToast.show to show toast.
        $mdToast.show({
            controller: 'toastController',
            templateUrl: 'toast.html',
            hideDelay: 800,
            position: 'top',
            locals: {
                displayOption: {
                    title: 'Going to ' + menuName + " !!"
                }
            }
        });
    }// End showToast.
});// End of controller menu dashboard.