// Controller of menu dashboard page.
appControllers.controller('incidentsDashboardCtrl', function ($scope, $mdToast,$mdBottomSheet,IncidentService,$mdDialog,$stateParams,$timeout,$ionicHistory,$ionicViewSwitcher,$state,$log,CommentaireService) {
    //ShowToast for show toast when user press button.
    $scope.addNewIncidentStepOne = function(){
        //  var stateParams = { typeIncident: params.value };
        //console.log(stateParams);
        $state.go('app.addIncident');
    }
    console.log($stateParams);
    $scope.list = false;

    var self = $scope;

    self.simulateQuery = false;
    self.isDisabled    = false;

    // list of `state` value/display objects
    self.states        = loadAll();
    self.querySearch   = querySearch;
    self.selectedItemChange = selectedItemChange;
    self.searchTextChange   = searchTextChange;
    $scope.incidentIsUnlistedConfirmation=false;
    self.newState = newState;

    function newState(state) {
        alert("Sorry! You'll need to create a Constitution for " + state + " first!");
    }

    // ******************************
    // Internal methods
    // ******************************

    /**
     * Search for states... use $timeout to simulate
     * remote dataservice call.
     */
    function querySearch (query) {
        var results = query ? self.states.filter( createFilterFor(query) ) : self.states,
            deferred;
        if (self.simulateQuery) {
            deferred = $q.defer();
            $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
            return deferred.promise;
        } else {
            return results;
        }
    }

    function searchTextChange(text) {
        console.log('Text changed to ' + text);
    }

    function selectedItemChange(item) {
        $log.info('Item changed to ' + JSON.stringify(item));
    }

    /**
     * Build `states` list of key/value pairs
     */
    function loadAll() {
        var allStates = 'Ascenseur, Electricité, Dégats des eaux, Nettoyage, Dégradation, Autres';

        return allStates.split(/, +/g).map( function (state) {
            return {
                value: state.toLowerCase(),
                display: state
            };
        });
    }

    /**
     * Create filter function for a query string
     */
    function createFilterFor(query) {
        var lowercaseQuery = angular.lowercase(query);

        return function filterFn(state) {
            return (state.value.indexOf(lowercaseQuery) === 0);
        };

    }


    $scope.templates =
        [{ name: 'template1.html', url: 'templates/themes/incidents-dashboard/html/single-incident.html'},
         { name: 'template2.html', url: 'template2.html'}];
    $scope.template = $scope.templates[0];

    $scope.loadUserIncidents = function(){
        //  var stateParams = { typeIncident: params.value };
        //$state.go('app.addIncident');
        var index = 'userIndex';
        var key = $scope.loggedInUser.uid;
        var value = true;
        $scope.userIncidentsList = IncidentService.allByIndex(index,key,value);
        //console.log($scope.list,value);
    }
    $scope.paging={};// = false;
    $scope.paging.shouldLoadData = false;
    $scope.isLoading = false;

    $scope.navigateTo = function (stateName,objectData,id) {
        /*
        if ($ionicHistory.currentStateName() != stateName) {
            $ionicHistory.nextViewOptions({
                disableAnimate: false,
                disableBack: true
            });
*/
        //Next view animate will display in back direction
        //   $ionicViewSwitcher.nextDirection('back');
        //:console.log(objectData);
        $state.go(stateName, {
            item: objectData,
            id: id
        });
        // }
    }; // End of navigateTo.
    // doRefresh is for refresh feed.
    $scope.doRefresh = function (refresher) {
        console.log('doRefresh');
        $scope.paging.shouldLoadData = false;
        //$scope.getFeedData(false);

        $timeout(function(){
            if ($scope.isLoading == false) {
                //      $scope.isLoading = true;
                $scope.$broadcast('scroll.refreshComplete');
                //refresher.complete();
            }else{
                //                $scope.isLoading = true;

            }
            // $scope.isLoading = false;
            console.log('doRefresh3');

            // To stop loading progress.
            //  $scope.$broadcast('scroll.infiniteScrollComplete');
            //    $scope.isLoading = false;
        }, 1000);


    };// End doRefresh.

    // loadMore is for loading more feed.
    $scope.loadMore = function () {
        if ($scope.isLoading == false) {
            $scope.isLoading = true;
            if ($scope.paging.next == "") {
                //$scope.getFeedData(true);
            } else {
                //$scope.getNextFeedData();
            }
        }
    };// End loadMore.
    $scope.filteredIncidents = [];
    $scope.loadIncidents = function(){
        //  var stateParams = { typeIncident: params.value };
        //$state.go('app.addIncident');
        console.log($scope,$scope.currentUser,$scope.currentAuth);
        var index = 'immeubleIndex';
        var key = $scope.currentUser.immeuble;

        //var key = $scope.loggedInUser.uid;
        var value = true;
        $scope.list = IncidentService.allByIndex(index,key,value);
        //$scope.list = IncidentService.all();
        console.log('load',$scope.currentUser,$scope.list,value);

        function dynamicSort(property) {
            var sortOrder = 1;
            if(property[0] === "-") {
                sortOrder = -1;
                property = property.substr(1);
            }
            return function (a,b) {
                var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
                return result * sortOrder;
            }
        }
        $scope.list.$loaded().then(function(data){
            console.log('xx',data);
            //$scope.list.isloaded = true;
            //$scope.list.sort(dynamicSort("description"));

            angular.forEach($scope.list, function(element,key) {
                $scope.filteredIncidents.push(element);
                console.log('las',$scope.list[key].last_comment,key);
                if($scope.list[key].last_comment){


                    $scope.list[key].last_comment.$loaded().then(function(comment){
                        console.log('com',comment);
                        if(comment.length)
                            $scope.list[key].last_comment_details = CommentaireService.getByKey(comment[0].$id);
                        $scope.list[key].comments_amount = comment.length;

                    });
                }
            });
        });

    }
    $scope.existingIncidents = [];
    $scope.loadUserIncidents();
    $scope.loadIncidents();
    //console.log();
    // For show show List Bottom Sheet.
    $scope.showListBottomSheet = function ($event) {
        $mdBottomSheet.show({
            templateUrl: 'ui-list-bottom-sheet-templatez',
            targetEvent: $event,
            scope: $scope.$new(false),
        });
    };// End of showListBottomSheet.

    // For show Grid Bottom Sheet.
    $scope.showGridBottomSheet = function ($event) {
        console.log('ok');
        $mdBottomSheet.show({
            templateUrl: 'ui-grid-bottom-sheet-templatez',
            targetEvent: $event,
            scope: $scope.$new(false),
        });
    };// End of showGridBottomSheet.

    $scope.showToast = function (menuName) {
        //Calling $mdToast.show to show toast.
        $mdToast.show({
            controller: 'toastController',
            templateUrl: 'toast.html',
            hideDelay: 800,
            position: 'top',
            locals: {
                displayOption: {
                    title: 'Going to ' + menuName + " !!"
                }
            }
        });
    }// End showToast.
}).controller('singleIncidentPageCtrl', function ($scope, $mdToast,$state,$stateParams,$mdBottomSheet,$mdDialog,IncidentService,$ionicSlideBoxDelegate,$timeout,CommentaireService) {

    $scope.form = {};
    $scope.item = $stateParams.item;
    $scope.comments = [];
    $scope.canEditEvent = false;
    if($scope.item.user === $scope.currentUser.id){
        $scope.canEditEvent = true;

    }
    $scope.loadIncidentCommentaires = function(){
        //  var stateParams = { typeIncident: params.value };
        //$state.go('app.addIncident');
        var index = 'incidentIndex';
        var key = $stateParams.id;
        var value = true;
        $scope.incidentCommentairesList = CommentaireService.allByIndex(index,key,value);
        console.log($scope.list,value);

        $scope.incidentCommentairesList.$loaded().then(function(){
            angular.forEach($scope.incidentCommentairesList, function(element) {
                $scope.comments.push(element);
            });
        });

        console.log($scope);

    }

    $scope.loadIncidentCommentaires();

    // $scope.comments.push({content:'ok',author:{name:'Jean'}});
    console.log('single',$scope.item,$state,$stateParams);
    //$stateParams.id=33;
    // Loading progress.
    $scope.$on( "$ionicView.enter", function( scopes, states ) {
        if( states.fromCache && states.stateName == "your view" ) {
            // do whatever
        }

        $scope.loadPage();
    });
    $scope.addComment=function(data){
        //$scope.form = {};
        //$scope.form.content = data.comment;
        $scope.form.user = $scope.loggedInUser.uid;
        //console.log($scope);
        $scope.form.incident = $scope.item.key;
        $scope.form.immeuble = $scope.item.immeuble;
        $scope.form.batiment = $scope.item.batiment;
        $scope.form.user = $scope.loggedInUser.uid;
        $scope.isEditCommentForm = false;
        //$scope.form.image = $scope.myImage ;

        console.log($scope,$scope.form);
        $scope.form.links={};
        //return;
        if($scope.isEditCommentForm){
            CommentaireService.save($stateParams.id,$scope.form);
        }else{
            CommentaireService.add($scope.form);

        }

        $scope.form = null;
        $scope.form = {};
        $scope.loadIncidentCommentaires();

    }
    $scope.loadPage = function(){
        $timeout(function () {
            if ($scope.isAndroid) {
                jQuery('#incident-post-loading-progress').show();
            }
            else {
                jQuery('#incident-post-loading-progress').fadeIn(700);
            }
        }, 400);
        $timeout(function () {
            jQuery('#incident-post-loading-progress').hide();
            jQuery('#wordpress-post-content').fadeIn();
        }, 1000);// End loading progress. 
    }


}).controller('addIncidentDashboardCtrl', function ($scope, $mdToast,$state,$stateParams,$mdBottomSheet,$mdDialog,IncidentService,$ionicSlideBoxDelegate,Camera) {
    //ShowToast for show toast when user press button.
    // var typeIncident = $state.params.typeIncident;
    //console.log($state.params,typeIncident);


    $scope.addNewIncident = function(params){
        var stateParams = { typeIncident: params.value };
        //console.log(stateParams);
        $state.go('app.addIncidentStepTwo',stateParams);
    }
    $scope.nextSlide = function() {
        $ionicSlideBoxDelegate.next();
    };
    $scope.previousSlide = function() {
        $ionicSlideBoxDelegate.previous();
    };
    $scope.goToSlide = function(i){
        //var stateParams = { typeIncident: params.value };
        //console.log(stateParams);
        $ionicSlideBoxDelegate.slide(i, [10])
    }
    $scope.isEditForm=false;
    $scope.initialForm = function () {
        if($stateParams.id!==null && $stateParams.id!==undefined){
            $scope.isEditForm=true;
        }
        // $scope.disableSaveBtn is  the variable for setting disable or enable the save button.
        $scope.disableSaveBtn = false;
        // $scope.contract is the variable that store contract detail data that receive form contract list page.
        // Parameter :  
        // $stateParams.actionDelete(bool) = status that pass from contract list page.
        // $stateParams.contractdetail(object) = contract that user select from contract list page.
        $scope.form ={titre:'',description:''};// $scope.getContractData($stateParams.actionDelete, $stateParams.contractdetail);
        $scope.form.batiment = $scope.currentUser.batiment;
        $scope.form.immeuble = $scope.currentUser.immeuble;
        $scope.form.status = 'open';
        //$scope.form.image = $scope.myImage ;
        if($scope.isEditForm){
            console.log('stayt',$state,$stateParams,$stateParams.id);

            IncidentService.getByKey($stateParams.id).$loaded().then(function(data){
                $scope.form = data[$stateParams.id];
                console.log($scope.form);
            });

        }

        //$scope.actionDelete is the variable for allow or not allow to delete data.
        // It will allow to delete data when have data in the database.
        $scope.actionDelete = $stateParams.actionDelete;
    }; //End initialForm.

    $scope.getIncidentData = function (id) {
        // tempContract is  temporary contract data detail.
        var tempContract = {
            id: null,
            firstName: '',
            lastName: '',
            telephone: '',
            email: '',
            createDate: $filter('date')(new Date(), 'MMM dd yyyy'),
            age: null,
            isEnable: false
        }
        // If actionDelete is true Contract Detail Page will show contract detail that receive form contract list page.
        // else it will show tempContract for user to add new data.
        return (actionDelete ? angular.copy(contractDetail) : tempContract);
    };//End get contract detail data.


    $scope.inputPicture = {"from":0,"type":"picture"};
    $scope.takePicture = function(){
       // $ionicTabsDelegate.select(1);
        var options = {
            quality:75,
            targetWidth:720,
            targetHeight:1280,
            destinationType:0
        };
        Camera.getPicture(options).then(function(imageData) {
            $scope.inputPicture.content = "data:image/jpeg;base64,"+imageData;
        }, function(err) {
            console.log(err);
        });
    };
    $scope.sendPicture = function(){
        if(angular.isDefined($scope.inputPicture.content)){
            var now = new Date().getTime();
            $scope.inputPicture.time = now;
            DetailMessages($localStorage.userLogin.id).post($stateParams.id,$scope.inputPicture);
            $scope.Messages = {};
            $scope.Messages.content = "[picture]";
            $scope.Messages.time = $scope.inputPicture.time;
            $scope.Messages.unread = 0;
            Messages($localStorage.userLogin.id).post($stateParams.id,$scope.Messages);
            Notification().post($stateParams.id);
            $scope.inputPicture = {"from":0,"type":"picture"};
        } else $scope.takePicture();
    };

    $scope.showInputImages = function(){
        var options = {
            sourceType:0,
            destinationType:0
        };
        Camera.getPicture(options).then(function(imageData) {
            $scope.inputPicture.content = "data:image/jpeg;base64,"+imageData;
            $ionicTabsDelegate.select(1);
        }, function(err) {
            console.log(err);
        });
    };

    $scope.saveIncident = function (form, $event) {
        //$mdBottomSheet.hide() use for hide bottom sheet.
        $mdBottomSheet.hide();
        //mdDialog.show use for show alert box for Confirm to save data.
        $mdDialog.show({
            controller: 'DialogController',
            templateUrl: 'confirm-dialog.html',
            targetEvent: $event,
            locals: {
                displayOption: {
                    title: "Confirm to save incident?",
                    content: "Data will save to Fb.",
                    ok: "Confirmer",
                    cancel: "Fermer"
                }
            }
        }).then(function () {
            $scope.form.user = $scope.loggedInUser.uid;
            //$scope.form.image = $scope.myImage ;

            console.log($scope,$scope.form);
            $scope.form.links={};

            if($scope.isEditForm){
                IncidentService.save($stateParams.id,$scope.form);
            }else{
                IncidentService.add($scope.form);

            }
            // $scope.form={};
            $state.go('app.listIncidents');
            return ;
            // For confirm button to save data.
            try {
                // To update data by calling ContractDB.update(contract) service.
                if ($scope.actionDelete) {
                    if ($scope.contract.id == null) {
                        $scope.contract.id = $scope.contractList[$scope.contractList.length - 1].id;
                    }
                    ContractDB.update(contract);
                } // End update data. 

                // To add new data by calling ContractDB.add(contract) service.
                else {
                    ContractDB.add(contract);
                    $scope.contractList = ContractDB.all();
                    $scope.actionDelete = true;
                }// End  add new  data. 

                // Showing toast for save data is success.
                $mdToast.show({
                    controller: 'toastController',
                    templateUrl: 'toast.html',
                    hideDelay: 400,
                    position: 'top',
                    locals: {
                        displayOption: {
                            title: "Data Saved !"
                        }
                    }
                });//End showing toast.
            }
            catch (e) {
                // Showing toast for unable to save data.
                $mdToast.show({
                    controller: 'toastController',
                    templateUrl: 'toast.html',
                    hideDelay: 800,
                    position: 'top',
                    locals: {
                        displayOption: {
                            title: window.globalVariable.message.errorMessage
                        }
                    }
                });//End showing toast.
            }
        }, function () {
            // For cancel button to save data.
        });// End alert box.
    };// End save contract.

    $scope.showToast = function (menuName) {
        //Calling $mdToast.show to show toast.
        $mdToast.show({
            controller: 'toastController',
            templateUrl: 'toast.html',
            hideDelay: 800,
            position: 'top',
            locals: {
                displayOption: {
                    title: 'Going to ' + menuName + " !!"
                }
            }
        });
    }// End showToast.

    $scope.deleteContract = function (contract, $event) {
        //$mdBottomSheet.hide() use for hide bottom sheet.
        $mdBottomSheet.hide();
        //mdDialog.show use for show alert box for Confirm to delete data.
        $mdDialog.show({
            controller: 'DialogController',
            templateUrl: 'confirm-dialog.html',
            targetEvent: $event,
            locals: {
                displayOption: {
                    title: "Confirm to remove data?",
                    content: "Data will remove form SQLite.",
                    ok: "Confirm",
                    cancel: "Close"
                }
            }
        }).then(function () {
            // For confirm button to remove data.
            try {
                // Remove contract by calling ContractDB.remove(contract)service.
                if ($scope.contract.id == null) {
                    $scope.contract.id = $scope.contractList[$scope.contractList.length - 1].id;
                }
                ContractDB.remove(contract);
                $ionicHistory.goBack();
            }// End remove contract.
            catch (e) {
                // Showing toast for unable to remove data.
                $mdToast.show({
                    controller: 'toastController',
                    templateUrl: 'toast.html',
                    hideDelay: 800,
                    position: 'top',
                    locals: {
                        displayOption: {
                            title: window.globalVariable.message.errorMessage
                        }
                    }
                });// End showing toast.
            }
        }, function () {
            // For cancel button to remove data.
        });// End alert box.
    };// End remove contract.

    // validateRequiredField is for validate the required field.
    // Parameter :  
    // form(object) = contract object that presenting on the view.
    $scope.validateRequiredField = function (form) {
        return !(   (form.titre.$error.required == undefined)
                 && (form.description.$error.required == undefined)
                );
    };// End validate the required field. 

    // showListBottomSheet is for showing the bottom sheet.
    // Parameter :  
    // $event(object) = position of control that user tap.


    // contractForm(object) = contract object that presenting on the view.
    $scope.showAddIncidentBottomSheet = function ($event, contractForm) {
        console.log('sho');
        $scope.disableSaveBtn = $scope.validateRequiredField(contractForm);
        $mdBottomSheet.show({
            templateUrl: 'incident-actions-template.html',
            targetEvent: $event,
            scope: $scope.$new(false),
        });
    };// End showing the bottom sheet.
    $scope.showListBottomSheet = function ($event, noteForm) {

        $scope.disableSaveBtn = $scope.validateRequiredField(noteForm);

        $mdBottomSheet.show({
            templateUrl: 'contract-actions-template',
            targetEvent: $event,
            scope: $scope.$new(false),
        });
    };// End showing the bottom sheet.
    $scope.initialForm();
}).controller('addIncidentStepTwoCtrl', function ($scope, $mdToast,$state,$stateParams,Poster,$q) {
    //ShowToast for show toast when user press button.
    var stateParams=$stateParams;
    // var typeIncident = $state.params.typeIncident;
    //console.log($state.params,typeIncident);
    /*
    $scope.incidentTypes=[];
    var type = {name:'Rez-de-chaussée',value:'ascenseur',icon:'fa-check-square-o'};
    var type2 = {name:'Cave',value:'electricite',icon:'fa-square-o'};
    var type3 = {name:'Etages',value:'degats-eaux',icon:'fa-square-o'};
    $scope.incidentTypes.push(type);
    $scope.incidentTypes.push(type2);
    $scope.incidentTypes.push(type3);
    $scope.addNewIncident = function(params){
        // var stateParams = { typeIncident: params.value };
        console.log(stateParams);
        $state.go('app.addIncidentStepTwo',{ typeIncident: params.value });
    }
    $scope.showToast = function (menuName) {
        //Calling $mdToast.show to show toast.
        $mdToast.show({
            controller: 'toastController',
            templateUrl: 'toast.html',
            hideDelay: 800,
            position: 'top',
            locals: {
                displayOption: {
                    title: 'Going to ' + menuName + " !!"
                }
            }
        });
    }// End showToast.
*/
    // $scope.currentFile={name:'xx'};
    //var _this = $scope;
    $scope.myCroppedImage = '';
    $scope.picThumb = '';
    $scope.canAddPic = true;
    $scope.canEditPic = false;
    $scope.canSavePic = false;
    $scope.hasSavePic = false;
    console.log('Ctr2',$scope);
    $scope.performClick = function (elemId) {
        console.log('ok');
        var elem = document.getElementById(elemId);
        if (elem && document.createEvent) {
            var evt = document.createEvent("MouseEvents");
            evt.initEvent("click", true, false);
            elem.dispatchEvent(evt);
        }
    }
    this.handleFileSelect = function (files) {
        console.log('rrrr', files)
        //var files = evt.target.files; // FileList object
        // Loop through the FileList and render image files as thumbnails.
        for (var i = 0, f; f = files[i]; i++) {
            // Only process image files.
            if (!f.type.match('image.*')) {
                continue;
            }
            // _this.form.files = f;
            _this.currentFile = f;
            console.log(f);
            var reader = new FileReader();
            // Closure to capture the file information.
            reader.onload = (function (theFile) {
                return function (e) {
                    console.log('onload', files)
                    document.getElementById('newPictureContainer').src = e.target.result;
                    //_this.form.cover = e.target.result;
                    // Render thumbnail.
                    /*
                        var span = document.createElement('span');
                        span.innerHTML = ['<img class="thumb" src="', e.target.result
                                , '" title="', escape(theFile.name), '"/>'].join('');
                        document.getElementById('list').insertBefore(span, null);
                        */
                };
            })(f);
            // Read in the image file as a data URL.
            reader.readAsDataURL(f);
        }
    }
    $scope.performFileInputChange = function (elemId) {
        console.log('performFileInputChange')
        $scope.hasSavePic = false;
        $scope.canAddPic = false;
        var files = angular.element(elemId)[0].files;
        // var image = angular.element('croppie');
        var image = angular.element(document.querySelector("#croppie img"));
        var opts = {};
        var args = {};
        //    var c = new Croppie(document.querySelector("#croppie img"), {});
        // console.log(c); //var c = new Croppie(document.getElementById('croppie'), opts);
        // call a method
        //c.method(args);
        /*
                    var media = angular.element(elemId)[0].files[0];
                    var vendorURL = window.URL || window.webkitURL;
                    var video = vendorURL.createObjectURL(media);
                    console.log('change', video, $scope.test, angular.element(elemId), media);
                    $scope.test = video;
                    takepicture();
                    */
        //    Poster.handleFileSelect(files);
        // var handleFileSelect=function(evt) {
        var file = files[0];
        var reader = new FileReader();
        reader.onload = function (evt) {
            $scope.$apply(function ($scope) {
                $scope.myImage = evt.target.result;
                console.log($scope.myImage);
                $scope.form.image64 = $scope.myImage;
                $scope.canSavePic = true; //  console.log(image, image[0], c.croppie());
                //image.croppie();
            });
        };
        reader.readAsDataURL(file);
    };
    $scope.resizePic = function (myCroppedImage) {
        // console.log('resize', myCroppedImage, $scope.myCroppedImage);
        //  $scope.$apply(function ($scope) {
        $scope.picThumb = myCroppedImage;
        $scope.canSavePic = false;
        $scope.canEditPic = true;
        $scope.hasSavePic = true;
        // });
    }
    $scope.uploadPic = function () {
        //console.log('resize', myCroppedImage, $scope.myCroppedImage);
        //$scope.picThumb = myCroppedImage;
    }
    $scope.resetPic = function () {
        //console.log('resize', myCroppedImage, $scope.myCroppedImage);
        //$scope.picThumb = myCroppedImage;
        $scope.picThumb = '';
        $scope.canSavePic = true;
        $scope.canEditPic = false;
        $scope.hasSavePic = false;
    }
    this.saveImage = function (data) {
        var picData = {
            type: 'images',
            name: 'zz'//this.currentFile.name
        }
        angular.extend(picData, data);
        console.log('saveimg', picData, data);
        return this.generateImages().then(function (pics) {
            return _this.storeImage(pics.full, pics.thumb, picData).then(function (postId) {
                return postId;
            });
        });
    }
    var _this = this;
    this.generateImages = function () {
        var _this = this;
        // console.log('$q', $q.defer());
        var fullDeferred = new $q.defer(); // $.Deferred(); //$q.defer(); //new $
        var thumbDeferred = new $q.defer(); //$.Deferred(); //new $.Deferred();
        var resolveFullBlob = function (blob) {
            console.log(fullDeferred);
            return fullDeferred.resolve(blob);
        };
        var resolveThumbBlob = function (blob) {
            return thumbDeferred.resolve(blob);
        };
        var displayPicture = function (url) {
            var image = new Image();
            image.src = url;
            console.log('displayPicture')
            // Generate thumb.
            var maxThumbDimension = _this.THUMB_IMAGE_SPECS.maxDimension;
            var thumbCanvas = _this._getScaledCanvas(image, maxThumbDimension);
            thumbCanvas.toBlob(resolveThumbBlob, 'image/jpeg', _this.THUMB_IMAGE_SPECS.quality);
            // Generate full sized image.
            var maxFullDimension = _this.FULL_IMAGE_SPECS.maxDimension;
            var fullCanvas = _this._getScaledCanvas(image, maxFullDimension);
            fullCanvas.toBlob(resolveFullBlob, 'image/jpeg', _this.FULL_IMAGE_SPECS.quality);
        };
        var reader = new FileReader();
        reader.onload = function (e) {
            return displayPicture(e.target.result);
        };
        reader.readAsDataURL($scope.myImage);
        //console.log('this.currentFile', this.currentFile, fullDeferred)
        return fullDeferred.promise.then(function (results1) {
            return thumbDeferred.promise.then(function (results2) {
                console.log(this, results1, results2);
                // _this.uploadNewPic(results1, results2);
                return {
                    full: results1,
                    thumb: results2
                };
            });
        });
    };
    $scope.saveEvent = function (data) {
        console.log(data, $scope.myImage);
        //return;
        var picData = {
            type: 'images',
            name: 'aa'//$scope.currentFile.name
        }
        data = $scope.myImage;
        var saveImage = _this.saveImage(picData);
        //console.log('okok', uploadImage);
        return saveImage.then(function (pics) {
            // Upload the File upload to Firebase Storage and create new post.
            //  var data = angular.extend({}, form)
            console.log('saveEvent', data, pics);
            //return pics;
            data.cover = angular.extend({}, pics.urls);
            return _this.prepareImagePost(pics, picData).then(function (postKey) {
                // _this.form.cover = data;
                //Poster.saveImage(data);
                console.log('saveEventPicpostKey', postKey, picData);
                var image = {};
                var type = {};
                image[postKey] = true;
                type['images'] = true;
                data.images = image;
                data.types = type;
                console.log("before-preepareeventspost", data, postKey)
                return _this.prepareEventPost(data).then(function (postKey) {
                    console.log("preepareeventspost", data, postKey)
                    _this.setGeo(data.location.geo, postKey);
                    return postKey;
                });
            })
            //return _this.prepareImagePost(pics, picData);
            //return _this.prepareEventPost(urls, pic, thumb, picData, picRef, thumbRef);
            // var saveEvent = _this.addPost(data);
            // console.log('saveEvent', pics, saveEvent);
        })
        /*
                angular.extend(picData, data);
                console.log('saveEvent', picData, data);
                return this.generateImages().then(function (pics) {
                    return _this.storeImage(pics.full, pics.thumb, picData).then(function (postId) {
                        return postId;
                    });
                });
                */
    }
});// End of controller menu dashboard.