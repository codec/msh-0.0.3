// Controller of menu dashboard page.
appControllers.controller('annoncesDashboardCtrl', function ($scope, $mdToast,$mdBottomSheet) {
    //ShowToast for show toast when user press button.
    $scope.addNewIncidentStepOne = function(){
        //  var stateParams = { typeIncident: params.value };
        //console.log(stateParams);
        $state.go('app.addIncident');
    }

    // For show show List Bottom Sheet.
    $scope.showListBottomSheet = function ($event) {
        $mdBottomSheet.show({
            templateUrl: 'ui-list-bottom-sheet-templatez',
            targetEvent: $event,
            scope: $scope.$new(false),
        });
    };// End of showListBottomSheet.

    // For show Grid Bottom Sheet.
    $scope.showGridBottomSheet = function ($event) {
        console.log('ok');
        $mdBottomSheet.show({
            templateUrl: 'ui-grid-bottom-sheet-templatez',
            targetEvent: $event,
            scope: $scope.$new(false),
        });
    };// End of showGridBottomSheet.

    $scope.showToast = function (menuName) {
        //Calling $mdToast.show to show toast.
        $mdToast.show({
            controller: 'toastController',
            templateUrl: 'toast.html',
            hideDelay: 800,
            position: 'top',
            locals: {
                displayOption: {
                    title: 'Going to ' + menuName + " !!"
                }
            }
        });
    }// End showToast.
}).controller('addIncidentDashboardCtrlX', function ($scope, $mdToast,$state,$stateParams) {
    //ShowToast for show toast when user press button.

    // var typeIncident = $state.params.typeIncident;
    //console.log($state.params,typeIncident);
    $scope.incidentTypes=[];
    var type = {name:'Ascenseur',value:'ascenseur',icon:'fa-sort-amount-asc'};
    var type2 = {name:'Electricité',value:'electricite',icon:'fa-bolt'};
    var type3 = {name:'Dégats des eaux',value:'degats-eaux',icon:'fa-tint'};
    $scope.incidentTypes.push(type);
    $scope.incidentTypes.push(type2);
    $scope.incidentTypes.push(type3);

    $scope.addNewIncident = function(params){
        var stateParams = { typeIncident: params.value };
        //console.log(stateParams);
        $state.go('app.addIncidentStepTwo',stateParams);
    }
    $scope.showToast = function (menuName) {
        //Calling $mdToast.show to show toast.
        $mdToast.show({
            controller: 'toastController',
            templateUrl: 'toast.html',
            hideDelay: 800,
            position: 'top',
            locals: {
                displayOption: {
                    title: 'Going to ' + menuName + " !!"
                }
            }
        });
    }// End showToast.
}).controller('addIncidentStepTwoCtrlXXX', function ($scope, $mdToast,$state,$stateParams) {
    //ShowToast for show toast when user press button.
    var stateParams=$stateParams;
    // var typeIncident = $state.params.typeIncident;
    //console.log($state.params,typeIncident);
    $scope.incidentTypes=[];
    var type = {name:'Rez-de-chaussée',value:'ascenseur',icon:'fa-check-square-o'};
    var type2 = {name:'Cave',value:'electricite',icon:'fa-square-o'};
    var type3 = {name:'Etages',value:'degats-eaux',icon:'fa-square-o'};
    $scope.incidentTypes.push(type);
    $scope.incidentTypes.push(type2);
    $scope.incidentTypes.push(type3);
    $scope.addNewIncident = function(params){
        // var stateParams = { typeIncident: params.value };
        console.log(stateParams);
        $state.go('app.addIncidentStepTwo',{ typeIncident: params.value });
    }
    $scope.showToast = function (menuName) {
        //Calling $mdToast.show to show toast.
        $mdToast.show({
            controller: 'toastController',
            templateUrl: 'toast.html',
            hideDelay: 800,
            position: 'top',
            locals: {
                displayOption: {
                    title: 'Going to ' + menuName + " !!"
                }
            }
        });
    }// End showToast.
});// End of controller menu dashboard.