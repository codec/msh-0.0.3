appControllers.controller('digipostCtrl', function ($scope, Animate, $state, Utilities, DigipostService, PopupService, StorageService, $ionicPopup, DataService) { 
    console.log($scope.currentUser);
   /*--------------Slide--------------*/    
    $scope.lockSlide = function () {
        Animate.slide().enableSlide( false );
    }   
    $scope.index = 0; 
    $scope.slide = function () {
        if($scope.index == 0) {
            $state.go('app.homeDashboard');
        }else {
            Animate.slide().slide(0);
            $scope.index = 0;
        }
    }
    $scope.toEdit = function (edit) {
        if (edit == true) {
            $scope.card = $scope.postIts[$scope.cardIndex];
            $scope.newPost.content = $scope.card.content;
        }
        $scope.edit = edit;
        Animate.slide().slide(1);
        $scope.index = 1;
    }
    
    /*-------------------------------*/
/*TODO*/$scope.delete = function(post) {
        console.log(post, $scope.postIts[$scope.cardIndex]);
        DataService.delete(post);
        $scope.postIts.splice($scope.cardIndex, 1);
        Animate.loadStrt(2000)
        initCards($scope.postIts).then(function(res){
            Animate.loadEnd();
        }), function(error) {
            console.log(error);
            PopupService.show('showError');
        }
    }
    $scope.edit = false;
    //$scope.$on( "$ionicView.enter", function(scopes, states) {
        if($scope.postIts){
          console.log($scope.postIts);
          $scope.postIts = DigipostService.get();
          initCards($scope.postIts).then(function(success){
            console.log(success);
          }), function(e){
            console.log(e);
          }
        }else{
          new Promise(function(resolve, reject){
            $scope.postIts = [];
            Animate.loadStrt(30000);
            DataService.getDigiPosts($scope.currentUser).then(function(postIts){
              loadPostIts(postIts).then(function(result){   
                if (!nothingToDisplay.content){
                  DigipostService.set(result);
                }
                initCards(result).then(function(success){
                  Animate.loadEnd(); 
                  resolve(success);
                }), function(e){
                  console.log(e);
                  PopupService.show('showError');
                };
              }), function (err){
                PopupService.show('showError');
                console.log(err);
              };

            }), function(error){
              PopupService.show('showError');
              console.log(error);
            };
          }).catch(function(e){
            console.log(e);
            PopupService.show('showError');
          })    
        }   
    //});

    var nothingToDisplay = {};
    
    function loadPostIts(postIts){
        return new Promise(function(resolve, reject){
          if (postIts.length > 0) {
                //$scope.postIts = sortNewest(postIts);
                $scope.postIts = postIts.sort(function(a,b){
                    return a.timeStamp-b.timeStamp;
                });
                resolve($scope.postIts.reverse());           
          } else {
            nothingToDisplay = {
                content:"Aucun message n'est disponible. Vous avez quelque chose à communiquer aux résidents de votre immeuble, rédigez votre 'mokolé'!",
                noPosts: true
            }
            $scope.postIts.push(nothingToDisplay);
            resolve($scope.postIts);
          }
          
        }).catch(function(e) {
            console.log(e);
        });
    }

    function initCards(result){
        $scope.postIts = result;
        return new Promise(function(resolve){
            /*if ($scope.postIts.length > 1) {
                $scope.postIts.push(angular.extend({}, $scope.postIts[0]));
            }*/
            $scope.cards = Array.prototype.slice.call($scope.postIts, 0);
            $scope.tempCard = $scope.postIts[$scope.postIts.length-1];
            console.log($scope.tempCard);
            resolve($scope.cards)
        }).catch(function(e){
            console.log(e);
            PopupService.show('showError');
        })
    }

    $scope.newPost = {
        content: "",
        creator: $scope.currentUser.formatedName,
        creatorId: $scope.currentUser.id,
        timeStamp: new Date().getTime() 
    } 

    $scope.post = function(){    
        var oldCard;
        if ($scope.newPost.content.length >= 5) {
            var persistTimeStamp = StorageService.get('persistTimeStamp');
            var timeBetweenTwoPosts = (parseInt(new Date().getTime()) - parseInt(persistTimeStamp))/1000;
            if ($scope.postIts.length > 0 && timeBetweenTwoPosts < 10 && $scope.edit == false) { // 600 correspond au nombre de secondes que l'utilisateur doit attendre avant de pouvoir poster un nouveau 'mokolé'
                var timeBeforeNextPost = Math.round((600 - timeBetweenTwoPosts)/60); // le nombre de minutes avant que l'utilisateur puisse poster de nouveau
                PopupService.show('showLimitedMokolesPerHour', timeBeforeNextPost);
            } else {  
                var head; var body;
                if ($scope.edit == false) {
                    oldCard = false;
                    head = 'Vous postez un nouveau mot.';
                    body = "Le contenu de ce mot sera accessible à l'ensembe de votre copropriété. confirmez-vous l'action?";
                } else {
                    oldCard = $scope.card;
                    head = 'Vous modifier votre mot.';
                    body = "Validez-vous les modifications apportées?";
                }
                var confirmPopup = $ionicPopup.confirm({
                    title: head,
                    template: body
                });
                confirmPopup.then(function(res) {
                    if (res) {
                        DigipostService.add($scope.postIts, $scope.newPost, oldCard).then(function(result){
                            DigipostService.edit($scope.newPost, $scope.currentUser, oldCard).then(function(newPost) {
                                if ($scope.cards[0] && $scope.cards[0].noPosts == true) {
                                    $scope.postIts.splice(0, 1, newPost);

                                }else if ($scope.edit == true) {
                                    var a = $scope.cards.indexOf(oldCard);
                                    $scope.postIts.splice(a, 1);
                                }else {
                                    $scope.postIts.splice(0, 0, newPost);  
                                }
                                Animate.loadStrt(2000);
                                initCards($scope.postIts).then(function(r) {
                                    console.log(r, r.length);
                                    Animate.loadEnd();
                                }), function(error) {
                                    PopupService.show('showError');
                                };  
                                //$scope.cards.push(angular.extend({}, newPost));
                                $scope.edit = false;
                                Animate.slide().slide(0);
                            }), function(error) {
                                console.log(error);
                            };  
                        }), function(e){
                            console.log(e);
                        };
                    }
                });
            } 
        }
    }

    /*-------------SwipeCards---------------*/
    $scope.cardIndex = -1;
    $scope.rank = 1; 
  
    $scope.cardSwiped = function(card) {
        console.log(card);
        if ($scope.cardIndex < $scope.cards.length - 2){
            $scope.cardIndex++;
        }else{
            $scope.cardIndex = 0;
        }
        $scope.rank = $scope.cardIndex+1;
        $scope.addCard($scope.cardIndex);
        $scope.cardDestroyed();
    };


    $scope.cardDestroyed = function() {
        $scope.cards.splice($scope.cardIndex, 1);
    };

    $scope.addCard = function(cardIndex) {
        if($scope.tempCard == false) {
            var newCard = $scope.postIts[cardIndex];
            console.log(newCard);
            $scope.cards.push(angular.extend({}, newCard));
        } else {
            $scope.cards.push(angular.extend({}, newCard));
            $scope.cards.push(angular.extend({}, $scope.tempCard));
            $scope.tempCard = false;
        }
        
    }
    /*--------------------------------------*/
});

