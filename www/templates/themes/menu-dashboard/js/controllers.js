// Controller of menu dashboard page.
appControllers.controller('menuDashboardCtrl', function ($scope, $mdToast,$state, user,Auth ){
    //ShowToast for show toast when user press button.
    $scope.showToast = function (menuName) {
        //Calling $mdToast.show to show toast.
        $mdToast.show({
            controller: 'toastController',
            templateUrl: 'toast.html',
            hideDelay: 800,
            position: 'top',
            locals: {
                displayOption: {
                    title: 'Going to ' + menuName + " !!"
                }
            }
        });
    }// End showToast.

    $scope.loggedInUser =   user;
    console.log('user',user);
    Auth.$onAuthStateChanged(function(authData) {
        if (authData) {
            // Define signed user
            console.log("Signed in as :", authData.email);
           // $scope.loggedInUser = authData;
            // Close register modal in register case
            // $scope.closeRegister();
            // Redirect correct page
            console.log("Signed In");
            if($scope.register) {
                //$location.path('/slide');
            } else {
                /*
                $scope.userInfo = {
                            name: result.data.name,
                            first_name: result.data.first_name,
                            last_name: result.data.last_name,
                            email: result.data.email,
                            birthday: result.data.birthday,
                            link: result.data.link,
                            cover: result.data.cover,
                            pictureProfileUrl: "http://graph.facebook.com/" + result.data.id + "/picture?width=500&height=500",
                            gender: result.data.gender,
                            id: result.data.id,
                            access_token: $scope.accessToken
                        };
                        */
                // Store user profile information to localStorage service.

                // $location.path('/tab/photo');

                // $state.go('app.homeDashboard');

            }
        } else {
            console.log("Signed out");
          //  $state.go('app.fakeLogin');
           // $scope.loggedInUser = null;
        }
        // localStorage.set("LoggedInUser", $scope.loggedInUser);
    });
    console.log(Auth);
});// End of controller menu dashboard.